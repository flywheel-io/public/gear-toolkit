import json
import os
import tempfile
from pathlib import Path

import pytest
from dotty_dict import Dotty

from flywheel_gear_toolkit.exceptions import ManifestValidationError
from flywheel_gear_toolkit.utils.manifest import Manifest

TESTS_DIR = Path(__file__).parents[1]


def test_manifest_can_load_file(json_file):
    res = Manifest.get_manifest_from_file(json_file("manifest"))
    assert isinstance(res, dict)

    # Raise if not file at location
    with pytest.raises(FileNotFoundError):
        Manifest.get_manifest_from_file("/does/not/exist")

    # Load from current directory if no path
    cwd = os.getcwd()
    os.chdir(TESTS_DIR / "data/")
    manifest = Manifest()
    assert manifest._path == (TESTS_DIR / "data/manifest.json").absolute()
    os.chdir(cwd)

    # Raise if not file in cwd
    with pytest.raises(FileNotFoundError):
        _ = Manifest()


def test_manifest_init_from_file_or_dict(json_file):
    manifest = Manifest(manifest=json_file("manifest"))
    assert manifest._path == Path(json_file("manifest"))
    assert isinstance(manifest.manifest, Dotty)
    assert isinstance(manifest.schema, dict)

    # can also instantiate from dict
    manifest = Manifest(manifest={"name": "gear-test"})
    assert manifest._path is None
    assert isinstance(manifest.manifest, Dotty)


def test_manifest_can_get_value(json_file):
    manifest = Manifest(manifest=json_file("manifest"))
    version = manifest.get_value("version")
    assert version == "0.0.1"

    nested_value = manifest.get_value("custom.gear-builder.category")
    assert nested_value == "sample"

    nested_value = manifest["custom.gear-builder.category"]
    assert nested_value == "sample"


def test_can_get_docker_image_name_tag(json_file, caplog):
    manifest = Manifest(manifest=json_file("manifest"))
    dint = manifest.get_docker_image_name_tag()
    assert dint == "flywheel/sample:0.0.1"

    manifest = Manifest(manifest=json_file("manifest"))
    _ = manifest.manifest.pop("custom.gear-builder.image")
    dint = manifest.get_docker_image_name_tag()
    assert dint == "flywheel/sample:0.0.1"
    assert 'Defining image in "custom.docker-image" is deprecated' in caplog.messages[0]


def test_validate_raise_if_not_valid(json_file):
    manifest = Manifest(manifest=json_file("manifest"))
    _ = manifest.manifest.pop("license")
    with pytest.raises(ManifestValidationError) as exec:
        manifest.validate()
    assert "The manifest at" in str(exec.value)
    assert "is invalid" in str(exec.value)


def test_manifest_can_validate(json_file):
    # Test valid manifest pass
    manifest = Manifest(manifest=json_file("manifest"))
    assert manifest.is_valid()

    # Test valid if gear-builder.image not defined but docker-image is
    manifest = Manifest(manifest=json_file("manifest"))
    _ = manifest.manifest.pop("custom.gear-builder.image")
    assert manifest.is_valid()

    # Test not valid if gear-builder.image not defined but docker-image is not
    manifest = Manifest(manifest=json_file("manifest"))
    _ = manifest.manifest.pop("custom.gear-builder.image")
    _ = manifest.manifest.pop("custom.docker-image")
    assert not manifest.is_valid()

    # Test not valid is license is missing
    manifest = Manifest(manifest=json_file("manifest"))
    _ = manifest.manifest.pop("license")
    assert not manifest.is_valid()

    # Test not valid is version miss-matched
    manifest = Manifest(manifest=json_file("manifest"))
    manifest.manifest["version"] = "something-else"
    assert not manifest.is_valid()

    # Test not valid is docker images does not match
    manifest = Manifest(manifest=json_file("manifest"))
    manifest.manifest["custom.gear-builder.image"] = "wrong_version:1.1.1"
    assert not manifest.is_valid()

    # Test not valid if docker name and custom.gear-builder.image name do not match
    manifest = Manifest(manifest=json_file("manifest"))
    manifest.manifest["name"] = "wrong_name"
    assert not manifest.is_valid()

    # Test valid default values
    manifest = Manifest(json_file("manifest"))
    manifest.manifest["config"]["config2"]["default"] = "not-a-bool"
    errors = manifest._validate()
    assert "is not of type 'boolean'" in errors[0]

    # Test default match property enum
    manifest = Manifest(json_file("manifest"))
    manifest.manifest["config"]["config1"]["default"] = "choice-not-in-enum"
    errors = manifest._validate()
    assert "is not one of" in errors[0]

    # Test default and config cannot be set together
    manifest = Manifest(json_file("manifest"))
    manifest.manifest["config"]["config2"]["default"] = True
    manifest.manifest["config"]["config2"]["optional"] = True
    errors = manifest._validate()
    assert (
        "should not be valid under {'required': ['default', 'optional']}" in errors[0]
    )

    # Test required config option with no default
    manifest = Manifest(json_file("manifest"))
    manifest.manifest["config"]["config2"].pop("default")
    assert manifest.is_valid()


def test_manifest_can_save(caplog):
    manifest_d = {"name": "test"}
    manifest = Manifest(manifest=manifest_d)
    with tempfile.TemporaryDirectory() as tmp_dir:
        manifest.to_json(Path(tmp_dir) / "manifest.json", validate=False)
        with open(Path(tmp_dir) / "manifest.json", "r") as fp:
            saved_manifest = json.load(fp)
            assert saved_manifest == manifest_d

    # test to_json log warning if manifest not valid
    with tempfile.TemporaryDirectory() as tmp_dir:
        manifest.to_json(Path(tmp_dir) / "manifest.json")
        assert "The saved manifest.json is invalid" in caplog.messages[0]


def test_manifest_properties(json_file):
    manifest = Manifest(json_file("manifest"))
    with open(json_file("manifest"), "r") as fp:
        manifest_d = json.load(fp)
    assert manifest.author == manifest_d["author"]
    assert manifest.config == manifest_d["config"]
    assert manifest.description == manifest_d["description"]
    assert manifest.label == manifest_d["label"]
    assert manifest.inputs == manifest_d["inputs"]
    assert manifest.license == manifest_d["license"]
    assert manifest.name == manifest_d["name"]
    assert manifest.source == manifest_d["source"]
    assert manifest.url == manifest_d["url"]
    assert manifest.version == manifest_d["version"]
