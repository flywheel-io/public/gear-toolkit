import logging
from subprocess import CalledProcessError

import pytest

from flywheel_gear_toolkit.utils import install_requirements


def test_install_requirements_no_error(mocker):
    subprocess_mock = mocker.patch("subprocess.check_call")
    install_requirements("test")

    subprocess_mock.assert_called_once()


def test_install_requirements_exception(mocker, caplog):
    caplog.set_level(logging.INFO)
    subprocess_mock = mocker.patch("subprocess.check_call")
    subprocess_mock.side_effect = CalledProcessError(returncode=1, cmd="test")

    with pytest.raises(SystemExit):
        install_requirements("test")
        subprocess_mock.assert_called_once()

        assert len(caplog.record_tuples) == 1
        assert (
            caplog.records[0].message
            == "Could not install requirements, pip exit code 1"
        )
