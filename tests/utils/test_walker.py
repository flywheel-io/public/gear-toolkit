from unittest.mock import MagicMock

import flywheel
import pytest

import flywheel_gear_toolkit.utils.walker as walker


def test_depth_first_walker_project(finder):
    w = walker.Walker(
        MagicMock(container_type="project", subjects=finder([]), files=[])
    )
    containers = list(w.walk())
    assert len(containers) == 1
    assert containers[0].container_type == "project"


def test_depth_first_walker_subject(finder):
    w = walker.Walker(
        MagicMock(container_type="subject", sessions=finder([]), files=[])
    )
    containers = list(w.walk())
    assert len(containers) == 1
    assert containers[0].container_type == "subject"


def test_depth_first_walker_session(finder):
    w = walker.Walker(
        MagicMock(container_type="session", acquisitions=finder([]), files=[])
    )
    containers = list(w.walk())
    assert len(containers) == 1
    assert containers[0].container_type == "session"


def test_depth_first_walker_acquisition():
    w = walker.Walker(MagicMock(container_type="acquisition", files=[]))
    containers = list(w.walk())
    assert len(containers) == 1
    assert containers[0].container_type == "acquisition"


def test_depth_first_walker_children(fw_project):
    project = fw_project(n_subs=2)
    w = walker.Walker(project)

    containers = list(w.walk())
    assert len(containers) == 9
    assert containers[0].container_type == "project"
    assert containers[1].container_type == "subject"
    assert containers[2].container_type == "session"


def test_breadth_first_walker_children(fw_project):
    project = fw_project(n_subs=2)
    w = walker.Walker(project, depth_first=False)

    containers = list(w.walk())
    assert len(containers) == 9
    assert containers[0].container_type == "project"
    assert containers[1].container_type == "subject"
    assert containers[2].container_type == "subject"


@pytest.mark.parametrize(
    "to_add",
    [
        flywheel.Acquisition(),
        [flywheel.Acquisition(), flywheel.Acquisition()],
        [0, 1],
    ],
)
def test_walker_add(to_add):
    w = walker.Walker(
        MagicMock(container_type="session", acquisitions=lambda: [], files=[])
    )

    w.add(to_add)
    assert len(w.deque) == (1 + (len(to_add) if isinstance(to_add, list) else 1))


def test_walker_stop_level():
    w = walker.Walker(
        MagicMock(
            container_type="session",
            acquisitions=lambda: [MagicMock(container_type="acquisition")],
            files=[],
        ),
        stop_level="session",
    )
    containers = list(w.walk())
    assert len(containers) == 1
    assert containers[0].container_type == "session"


def test_walker_stop_level_error():
    cont = MagicMock()
    w = walker.Walker(cont, stop_level="file")
    assert w._exclude == []


def test_walker_reload(finder):
    acq = MagicMock(container_type="acquisition")
    acq.reload.return_value = acq
    sess = MagicMock(container_type="session", acquisitions=finder([acq]), files=[])
    sess.reload.return_value = sess
    w = walker.Walker(sess, reload=True)
    containers = list(w.walk())
    assert len(containers) == 2
    assert containers[0].container_type == "session"
    sess.reload.assert_called_once()
    acq.reload.assert_called_once()


def test_walker_callback(fw_project):
    def my_callback(cont):
        if cont.container_type != "session":
            return True
        else:
            return False

    project = fw_project(n_subs=2)
    w = walker.Walker(project)

    containers = list(w.walk(my_callback))

    assert len(containers) == 5
    assert all(
        [
            elem.container_type not in ["acquisition", "file", "analysis"]
            for elem in containers
        ]
    )
