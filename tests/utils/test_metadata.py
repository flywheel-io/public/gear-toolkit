import json
import logging
from contextlib import nullcontext as does_not_raise
from pathlib import Path
from unittest.mock import MagicMock

import numpy as np
import pytest

import flywheel_gear_toolkit
from flywheel_gear_toolkit.utils.metadata import Metadata

# pylint: disable=protected-access


@pytest.fixture
def metadata():
    context = MagicMock()
    # Give context one input file called test
    context.config_json = {
        "inputs": {
            "test": {
                "base": "file",
                "object": {"info": {}},
                "hierarchy": {"type": "acquisition"},
                "location": {"name": "test"},
            },
            "api-key": {"base": "api-key"},
        }
    }
    context.config = {"test": 1}
    context.manifest = {"name": "test-gear", "version": "0.1.0"}
    return Metadata(context=context)


def test_add_container_metadata(metadata):
    # Add basic
    info1 = {"my_val": "my-test"}
    metadata.update_container("acquisition", **{"info": info1, "type": "dicom"})
    assert metadata._metadata.get("acquisition").get("info") == info1
    # Update info
    info2 = {"val2": "test"}
    metadata.update_container("acquisition", **{"info": info2})
    assert metadata._metadata.get("acquisition").get("info") == {**info1, **info2}
    # Reset info with deep=False
    metadata.update_container("acquisition", deep=False, **{"info": {"val3": "test3"}})
    assert metadata._metadata.get("acquisition").get("info") == {"val3": "test3"}


@pytest.mark.parametrize(
    "existing, add, exp, deep",
    [
        ({}, {"qc": {"test1": "val1"}}, {"qc": {"test1": "val1"}}, True),
        # Merges
        (
            {"qc": {"test": "test1"}},
            {"qc": {"test1": "val1"}},
            {"qc": {"test": "test1", "test1": "val1"}},
            True,
        ),
        # Expected is just the added value when no existing info
        ({}, {"qc": {"test1": "val1"}}, {"qc": {"test1": "val1"}}, False),
        # Overwrite qc namespace with added value if deep is false
        (
            {"qc": {"test": "test1"}},
            {"qc": {"test1": "val1"}},
            {"qc": {"test1": "val1"}},
            False,
        ),
    ],
)
def test_add_file_metadata_file_name_matches_input(metadata, existing, add, exp, deep):
    filename = "file1.txt"
    metadata._context.config_json = {
        "inputs": {
            "input_1": {
                "base": "file",
                "object": {"info": existing},
                "hierarchy": {"type": "acquisition"},
                "location": {"name": filename},
            },
        }
    }
    # File name matches one of the provided inputs
    metadata.update_file_metadata(filename, deep=deep, **{"info": add})
    file_meta = metadata._metadata["acquisition"]["files"][0]
    assert file_meta["info"] == exp
    assert file_meta["name"] == filename


def test_add_file_metadata_file_name_matches_output(metadata, tmp_path):
    metadata._context.output_dir = tmp_path
    get_dest = metadata._context.get_destination_container
    get_dest.return_value.container_type = "acquisition"
    (tmp_path / "file1.txt").touch()
    qc = {"qc": {"test": "val"}}
    metadata.update_file_metadata("file1.txt", **{"info": qc})
    file_meta = metadata._metadata["acquisition"]["files"][0]
    assert file_meta["info"] == qc


def test_add_file_metadata_file_name_no_context(metadata):
    metadata._context = None
    with pytest.raises(ValueError):
        qc = {"qc": {"test": "val"}}
        metadata.update_file_metadata("test", **{"info": qc})


def test_add_file_metadata_file_name_no_match(metadata):
    with pytest.raises(RuntimeError):
        qc = {"qc": {"test": "val"}}
        metadata.update_file_metadata("no_file_match", **{"info": qc})


def test_add_file_metadata_config_json_obj(metadata):
    file_ = {
        "hierarchy": {"type": "acquisition"},
        "object": {"info": {"qc": {"test": "test1"}}},
        "location": {"name": "test"},
        "base": "file",
    }
    # Change value of qc.test
    qc = {"qc": {"test": "val"}}
    metadata.update_file_metadata(file_, **{"info": qc})
    file_meta = metadata._metadata["acquisition"]["files"][0]
    assert file_meta["info"] == qc


def test_add_file_metadata_sdk_file(metadata):
    file_ = {
        "name": "test",
        "parent_ref": {"type": "acquisition"},
        "info": {"qc": {"test": "test1"}},
    }
    qc = {"qc": {"test": "val"}}
    metadata.update_file_metadata(file_, **{"info": qc})
    file_meta = metadata._metadata["acquisition"]["files"][0]
    assert file_meta["info"] == qc


# A few general tests, some of these are tested above,
# but keeping these older ones to make it clear.
def test_update_file_support_multiple_calls(metadata):
    metadata.update_file_metadata("test", **{"type": "dicom"})
    metadata.update_file_metadata("test", **{"modality": "MR"})
    file_meta = metadata._metadata["acquisition"]["files"][0]
    assert file_meta["type"] == "dicom"
    assert file_meta["modality"] == "MR"


def test_update_file_supports_nested_update(metadata):
    metadata.update_file_metadata("test", **{"info": {"test1": 1}})
    metadata.update_file_metadata("test", **{"info": {"test2": 2}})
    file_meta = metadata._metadata["acquisition"]["files"][0]
    assert file_meta["info"]["test1"] == 1
    assert file_meta["info"]["test2"] == 2


def test_update_metadata_nested_update_can_be_disabled(metadata):
    initial_info = {"test1": 1}
    final_info = {"test2": 2}
    metadata.update_file_metadata("test", **{"info": initial_info})
    metadata.update_file_metadata("test", **{"info": final_info}, deep=False)
    file_meta = metadata._metadata["acquisition"]["files"][0]
    assert file_meta["info"] == final_info


def test_udpate_zip_member_count(metadata, zips):
    zip_dir = zips([("test1.zip", 2), ("test2.zip", 100)])
    metadata.update_zip_member_count(zip_dir, "analysis")

    files = metadata._metadata["analysis"]["files"]
    assert {"name": "test1.zip", "info": {}, "zip_member_count": 2} in files
    assert {"name": "test2.zip", "info": {}, "zip_member_count": 100} in files


def test_update_zip_member_count_log_warning_for_invalid_zipfile(
    metadata, caplog, tmp_path
):
    metadata._context._out_dir = tmp_path
    (tmp_path / "invalid.zip").touch()
    metadata.update_zip_member_count(tmp_path, "analysis")
    assert len(caplog.records) == 1
    assert "Invalid zip file" in caplog.records[0].message


def test_update_zip_member_count_file(metadata, zips):
    filename = "test2.zip"
    zip_dir = zips([(filename, 100)])
    zip_f = zip_dir / filename
    metadata.update_zip_member_count(zip_f, "analysis")

    files = metadata._metadata["analysis"]["files"]
    assert files[0]["name"] == filename
    assert files[0]["zip_member_count"] == 100


def test_update_zip_member_count_path_not_exists(metadata, caplog, tmp_path):
    with caplog.at_level(logging.INFO):
        metadata.update_zip_member_count(tmp_path / "does_not_exist", "analysis")
    last_msg = caplog.record_tuples[-1]
    assert last_msg[1] == logging.WARNING
    assert "Provided path does not exist" in last_msg[2]


def test_log_metadata_simple(caplog, metadata):
    metadata._metadata = {"ping": "pong"}
    with caplog.at_level(logging.INFO):
        metadata.log()
    assert caplog.records[0].levelname == "INFO"
    assert caplog.records[0].message == '.metadata.json:\n{\n  "ping": "pong"\n}'


def test_log_metadata_trims_long_lists(caplog, metadata):
    metadata._metadata = {"ping": ["1", "2", "3", "4", "5", "6", "7", "8"]}
    with caplog.at_level(logging.INFO):
        metadata.log()
    assert caplog.records[0].levelname == "INFO"
    assert caplog.records[0].message == (
        '.metadata.json:\n{\n  "ping": [\n    "1",\n    '
        '"...6 more items...",\n    "8"\n  ]\n}'
    )


@pytest.mark.parametrize(
    "fail_on_error,raises",
    [(True, pytest.raises(SystemExit)), (False, does_not_raise())],
)
def test_log_metadata_errors(metadata, fail_on_error, raises, tmp_path):
    metadata._metadata = {"acquisition": {"label": 1}}
    with raises:
        metadata.write(tmp_path, fail_on_validation=fail_on_error)


def test_context_meta_handles_bytes(tmp_path, metadata):
    metadata.update_file_metadata(
        "test",
        **{"type": "dicom"},
        info={
            "header": {
                "dicom": {
                    "DigitalSignaturesSequence": [
                        {
                            "CertificateOfSigner": (
                                b"0\x82\x01\xa5\xa2<\x12?\x07\xd3\x87.)\x195\t"
                            ),
                            "CertificateType": "X509_1993_SIG",
                            "DigitalSignatureUID": "1.2.826.0.1.2018.84.2600",
                            "MACIDNumber": 1,
                            "Signature": b",\xeb\xb8",
                        }
                    ]
                }
            },
            "nan": float("nan"),
            "inf": float("inf"),
            "float": 1.0,
            "numpy": np.array([1, 2, 3]),
        },
    )
    metadata.write(tmp_path)
    with open(tmp_path / ".metadata.json", "r") as fp:
        meta = json.load(fp)
        info = meta["acquisition"]["files"][0]["info"]
        dicom_head = info["header"]["dicom"]
        assert (
            dicom_head["DigitalSignaturesSequence"][0]["CertificateOfSigner"]
            == "308201a5a2 ... truncated byte value."
        )
        assert dicom_head["DigitalSignaturesSequence"][0]["Signature"] == "2cebb8"
        assert info["nan"] is None
        assert info["inf"] is None
        assert info["float"] == 1.0
        assert info["numpy"] == [1, 2, 3]


@pytest.mark.parametrize(
    "top_level, path, name, version, dest",
    [
        # Change namespace
        ("qc", ["qc"], "", "", "acquisition"),
        ("gear", ["gear"], "", "", "acquisition"),
        ("mykey.gear", ["mykey", "gear"], "", "", "acquisition"),
        # Override gear name and version
        ("qc", ["qc"], "test", "1.0.0", "acquisition"),
        # Change output destination
        ("qc", ["qc"], "test", "1.0.0", "analysis"),
        # No job id found
        ("qc", ["qc"], "test", "1.0.0", None),
    ],
)
def test_add_qc_info_namespace_qc_file(
    metadata, tmp_path, top_level, path, name, version, dest, caplog
):
    # Test a number of cases for add_qc_info, see parameters for cases
    context = metadata._context
    context.config_json = {
        "inputs": {
            "test": {
                "base": "file",
                "object": {"info": {}, "file_id": "123435", "version": 2},
                "hierarchy": {"type": "acquisition"},
                "location": {"name": "test"},
            },
        }
    }
    job_id = "111"
    context.config = {"test": 1}
    context.output_dir = Path(tmp_path)
    get_dest = context.get_destination_container
    get_dest.return_value.container_type = dest
    context.config_json["job"] = {"id": job_id}
    filename = "file1.txt"
    (tmp_path / filename).touch()
    qc = {"result": "value"}
    metadata.name_override = name
    metadata.version_override = version
    metadata.pull_job_info()
    with caplog.at_level(logging.INFO):
        metadata.add_qc_result(filename, name=top_level, state="PASS", **qc)
    file_ = metadata._metadata[dest]["files"][0]
    assert file_["name"] == filename
    gear_qc_info = file_.get("info").get("qc")
    exp = {
        (name if name else "test-gear"): {
            "job_info": {
                "version": (version if version else "0.1.0"),
                "job_id": job_id,
                "inputs": {
                    "test": {
                        "parent": {"type": "acquisition"},
                        "file_id": "123435",
                        "version": 2,
                        "file_name": "test",
                    }
                },
                "config": {"test": 1},
            },
            top_level: {"result": "value", "state": "PASS"},
        }
    }

    assert gear_qc_info == exp


def test_add_gear_info_analysis(metadata):
    metadata._context.destination = {"type": "analysis"}
    metadata._context.get_destination_container.return_value = {"job": {"id": "123"}}
    updated_file_info = metadata.add_gear_info("qc", "test", **{})
    job = updated_file_info["qc"]["test-gear"]["job_info"]["job_id"]
    assert job == "123"


def test_add_gear_info_analysis_no_sdk(metadata, caplog):
    metadata._context.destination = {"type": "analysis"}
    metadata._context.get_destination_container.side_effect = AttributeError()
    with caplog.at_level(logging.INFO):
        updated_file_info = metadata.add_gear_info("qc", "test", **{})
    assert caplog.record_tuples[-1][1] == logging.WARNING
    assert "Could not determine job id" in caplog.record_tuples[-1][2]
    assert caplog.record_tuples[-2][1] == logging.WARNING
    assert "Could not use SDK" in caplog.record_tuples[-2][2]
    job = updated_file_info["qc"]["test-gear"]["job_info"]["job_id"]
    assert job == ""


def test_add_gear_info_no_job_id(metadata, caplog):
    with caplog.at_level(logging.INFO):
        updated_file_info = metadata.add_gear_info("qc", "test", **{})
    last_msg = caplog.record_tuples[-1]
    assert last_msg[1] == logging.WARNING
    assert "Could not determine job id" in last_msg[2]
    job = updated_file_info["qc"]["test-gear"]["job_info"]["job_id"]
    assert job == ""

@pytest.fixture
def job_info():
    def inner(job="111", version="0.1.0", results={}, destination="acquisition"):
        info = {
            "test-gear": {
                "job_info": {
                    "version": version,
                    "job_id": job,
                    "inputs": {
                        "test": {
                            "parent": {"type": destination},
                            "file_id": "123435",
                            "version": 2,
                            "file_name": "test",
                        }
                    },
                    "config": {"test": 1},
                }
            }
        }
        info["test-gear"].update(results)
        return info

    return inner


@pytest.mark.parametrize("destination", ["acquisition", "analysis"])
@pytest.mark.parametrize(
    "job,version,add,exp",
    [
        # Same job, update qc results
        (
            "111",
            "0.1.0",
            {"result2": "value"},
            {"results": {"result": "value", "result2": "value"}},
        ),
        (
            # Same job, different gear version (should be impossible.)
            # But regardless, same job, so job_info
            # shouldn't be updated, but qc should be
            "111",
            "0.2.0",
            {"result2": "value"},
            {"results": {"result": "value", "result2": "value"}},
        ),
        (
            # Different job, updates job_info and replaces qc
            "123",
            "0.2.0",
            {"result2": "value"},
            {"job": "123", "version": "0.2.0", "results": {"result2": "value"}},
        ),
    ],
)
def test_add_gear_info_set_or_update(
    metadata, tmp_path, job, version, add, exp, job_info, destination
):
    # Test set or update of add gear info
    inputs_ = {
        "api-key": {"base": "api-key"},
        "test": {
            "base": "file",
            "object": {
                "info": {
                    "qc": job_info(results={"result": "value"}, destination=destination)
                },
                "file_id": "123435",
                "version": 2,
            },
            "hierarchy": {"type": destination},
            "location": {"name": "test"},
        },
    }
    context = metadata._context
    context.config_json = {"inputs": inputs_, "job": {"id": job}}
    context.config = {"test": 1}
    context.output_dir = Path(tmp_path)
    get_dest = context.get_destination_container
    get_dest.return_value.container_type = destination
    get_dest.return_value.get.return_value = {"id": job}
    context.manifest = {"name": "test-gear", "version": version}
    assert not metadata._has_job_info
    metadata.pull_job_info()
    assert metadata._has_job_info
    updated_file_info = metadata.add_gear_info("qc", inputs_["test"], **add)
    file_qc = updated_file_info["qc"]
    exp_res = job_info(**exp, destination=destination)
    assert file_qc == exp_res


def test_add_gear_info_no_job_ids(metadata, tmp_path, job_info):
    # Test that job_info updates when job_ids not populated
    inputs_ = {
        "api-key": {"base": "api-key"},
        "test": {
            "base": "file",
            "object": {
                "info": {
                    "qc": job_info(job="", results={"result": "value"}, destination="acquisition")
                },
                "file_id": "123435",
                "version": 2,
            },
            "hierarchy": {"type": "acquisition"},
            "location": {"name": "test"},
        },
    }
    context = metadata._context
    context.config_json = {"inputs": inputs_, "job": {"id": ""}}
    context.config = {"test": 1}
    context.output_dir = Path(tmp_path)
    get_dest = context.get_destination_container
    get_dest.return_value.container_type = "acquisition"
    get_dest.return_value.get.return_value = {"id": ""}
    context.manifest = {"name": "test-gear", "version": "0.1.1"}
    assert not metadata._has_job_info
    metadata.pull_job_info()
    assert metadata._has_job_info
    updated_file_info = metadata.add_gear_info("qc", inputs_["test"], **{"result2": "value"})
    file_qc = updated_file_info["qc"]
    assert file_qc["test-gear"]["job_info"]["version"] == "0.1.1"

@pytest.mark.parametrize("kwargs", [{}, {"test1": "val"}])
def test_add_gear_info_no_context(metadata, caplog, kwargs):
    metadata._context = None
    metadata.pull_job_info()
    qc = {"qc": {"test": "test1"}}
    file_ = {
        "hierarchy": {"type": "acquisition"},
        "object": {"info": qc},
        "location": {"name": "test"},
        "base": "file",
    }
    with caplog.at_level(logging.INFO):
        updated_file_info = metadata.add_gear_info("qc", file_, **kwargs)
    last_msg = caplog.record_tuples[-1]
    assert last_msg[1] == logging.INFO
    assert "Context not provided" in last_msg[2]

    qc["qc"].update(kwargs)
    assert updated_file_info == qc

@pytest.mark.parametrize("state", ['paSS', 'Fail', 'nA'])
def test_add_qc_result(metadata, tmp_path, state):
    metadata._context.output_dir = tmp_path
    gear_name = "test-gear"
    gear_version = "0.1.0"
    metadata._context.manifest = {"name": gear_name, "version": gear_version}
    metadata._context.config_json = {"inputs": {}}
    metadata._context.config = {}
    (tmp_path / "output.txt").touch()
    get_dest = metadata._context.get_destination_container
    get_dest.return_value.container_type = "acquisition"
    metadata.pull_job_info()
    data = {"dat": "val"}
    metadata.add_qc_result("output.txt", "result_1", state, **data)
    file_qc = metadata._metadata["acquisition"]["files"][0]["info"]["qc"]
    exp = {
        gear_name: {
            "job_info": {
                "version": gear_version,
                "job_id": "",
                "inputs": {},
                "config": {},
            },
            "result_1": {"state": state.upper(), **data},
        }
    }
    assert file_qc == exp


def test_add_qc_result_raises(metadata):
    with pytest.raises(ValueError):
        # soso is not a valid state.
        metadata.add_qc_result("test", "res", "soso")


@pytest.mark.parametrize(
    "existing, new, exp",
    [
        (None, "test", ["test"]),
        (None, ["test"], ["test"]),
        (None, None, []),
        (["test"], ["test"], ["test"]),
        (["test"], None, ["test"]),
        (["test", "my-test"], ["test"], ["test", "my-test"]),
        (
            ["test", "my-test"],
            ["test", "my-test", "test2"],
            ["test", "test2", "my-test"],
        ),
    ],
)
def test_add_file_tags(metadata, existing, new, exp):
    if existing:
        test = metadata._context.config_json["inputs"]["test"]
        test["object"]["tags"] = existing
    metadata.add_file_tags("test", new)
    file_ = metadata._metadata["acquisition"]["files"][0]
    assert len(file_["tags"]) == len(exp)
    assert sorted(file_["tags"]) == sorted(exp)


def test_sanitize_periods(metadata):
    # Generate nested dict with periods in the keys.
    input_dict = {
        "dict.1": {"nested.dict.1": {"nested.key.1": "nested.value.1"}},
        "dict.2": {"nested.key.2": "nested.value.2"},
        "key.3": "value.3",
    }
    # Generate nested dict with underscores in the keys.
    expected_dict = {
        "dict_1": {"nested_dict_1": {"nested_key_1": "nested.value.1"}},
        "dict_2": {"nested_key_2": "nested.value.2"},
        "key_3": "value.3",
    }

    output_dict = flywheel_gear_toolkit.utils.metadata.sanitize_periods(input_dict)
    assert output_dict == expected_dict
