import dataclasses
import json
import logging
import multiprocessing
import os
from contextlib import nullcontext as does_not_raise
from pathlib import Path

import pytest

from flywheel_gear_toolkit.utils.reporters import (
    AggregatedReporter,
    BaseLogRecord,
    LogRecord,
)


@pytest.mark.parametrize(
    "val, raises", [[1, pytest.raises(TypeError)], ["1", does_not_raise()]]
)
def test_base_log(val, raises):
    with raises:
        x = LogRecord(container_type=val)


def test_base_log_helpers():
    x = LogRecord()
    assert x.keys() == LogRecord.keys()
    assert isinstance(x.values(), tuple)
    assert isinstance(x.to_dict(), dict)


@pytest.fixture
def out_path(tmp_path):
    def _gen(o_type):
        out_path = Path(tmp_path) / ("test." + o_type)
        out_path.touch()
        return out_path

    return _gen


class LogRecord_01(BaseLogRecord):
    container_tye: str


@dataclasses.dataclass
class LogRecord_02(BaseLogRecord):
    container_tye: str


@dataclasses.dataclass
class LogRecord_03:
    container_tye: str


@pytest.mark.parametrize(
    "format,error", [(LogRecord_01, ValueError), (LogRecord_03, TypeError)]
)
def test_format_invalid(out_path, format, error):
    o_path = out_path("csv")
    o_path.unlink()

    with pytest.raises(error):
        _ = AggregatedReporter(output_path=o_path, format=format)


@pytest.mark.parametrize(
    "o_type",
    [
        "csv",
        "json",
    ],
)
def test_init_aggregated_reporter(o_type, out_path):
    o_path = out_path(o_type)
    o_path.unlink()

    r = AggregatedReporter(output_path=o_path)

    assert r.output_path == out_path(o_type)
    assert r.output_type == o_type
    assert isinstance(r.format(), LogRecord)
    if o_type == "csv":
        r.__del__()
        with open(r.output_path, "r") as fp:
            lines = fp.readlines()
            assert len(lines) == 1
            assert (
                lines[0]
                == "container_type,container_id,label,err,msg,resolved,search_key\n"
            )


def test_init_output_exists(out_path):
    o_path = out_path("csv")
    with pytest.raises(ValueError):
        _ = AggregatedReporter(output_path=o_path)


def test_init_wrong_output_type(out_path):
    o_path = out_path("txt")
    with pytest.raises(ValueError):
        _ = AggregatedReporter(output_path=o_path)


@pytest.mark.parametrize("lvl", ["err", "msg"])
def test_append_log(mocker, lvl, out_path):
    o_path = out_path("csv")
    o_path.unlink()
    entry = "test"
    r = AggregatedReporter(output_path=o_path)

    write_log_mock = mocker.patch.object(r, "write_log")

    append_args = {
        "container_type": "test",
        "container_id": "test",
        "label": "test",
        "resolved": False,
        "search_key": "test",
    }
    append_args[lvl] = entry

    r.append_log(**append_args)
    write_log_mock.assert_called_once()
    log_args = write_log_mock.call_args[0][0]
    assert vars(log_args) == vars(LogRecord(**append_args))


def test_append_log_record(mocker, out_path):
    o_path = out_path("csv")
    o_path.unlink()
    r = AggregatedReporter(output_path=o_path)

    write_log_mock = mocker.patch.object(r, "write_log")

    rec = LogRecord(
        container_type="test",
        container_id="test",
        label="test",
        resolved=False,
        search_key="test",
        msg="test",
    )

    r.append_log(rec)
    write_log_mock.assert_called_once_with(rec)


def test_append_log_both(out_path):
    o_path = out_path("csv")
    o_path.unlink()
    r = AggregatedReporter(output_path=o_path)

    append_args = {
        "container_type": "test",
        "container_id": "test",
        "label": "test",
        "resolved": False,
        "search_key": "test",
        "msg": "test",
    }
    rec = LogRecord(
        container_type="test",
        container_id="test",
        label="test",
        resolved=False,
        search_key="test",
        msg="test",
    )

    with pytest.raises(ValueError):
        r.append_log(rec, **append_args)


@pytest.mark.parametrize("format, err_log", [(None, False), (LogRecord_02, True)])
def test_append_log_non_default_keys(mocker, out_path, format, err_log, caplog):
    o_path = out_path("csv")
    o_path.unlink()
    r = None
    if format:
        r = AggregatedReporter(output_path=o_path, format=format)
    else:
        r = AggregatedReporter(output_path=o_path)

    mocker.patch.object(r, "write_log")

    caplog.set_level(logging.ERROR)
    append_args = {}
    r.append_log(**append_args)
    if err_log:
        assert len(caplog.record_tuples) >= 1
        assert any([rec[1] == logging.ERROR for rec in caplog.record_tuples])


@pytest.mark.parametrize("o_type", ["csv", "json"])
def test_write_log_output(o_type, out_path):
    o_path = out_path(o_type)
    o_path.unlink()
    r = AggregatedReporter(output_path=o_path)

    for i in range(10):
        r.append_log(
            container_type="session",
            label=f"test-{i}",
            container_id=str(i),
            resolved=False,
            search_key="",
            msg=("" if i % 2 == 0 else "msg-" + str(i)),
            err=("" if i % 2 != 0 else "err-" + str(i)),
        )
    r.__del__()

    with open(str(o_path), "r") as fp:
        if o_type == "csv":
            lines = fp.readlines()

            assert len(lines) == 11
            assert (
                lines[0]
                == "container_type,container_id,label,err,msg,resolved,search_key\n"
            )

            for i, line in enumerate(lines[1:]):
                assert line == (
                    f"session,{i},test-{i},"
                    + f"{'' if i % 2 != 0 else 'err-'+str(i)},"
                    + f"{'' if i % 2 == 0 else 'msg-'+str(i)},"
                    + "False,\n"
                )
        else:
            recs = json.load(fp)

            for i, record in enumerate(recs):
                assert record == {
                    "container_type": "session",
                    "label": f"test-{i}",
                    "container_id": str(i),
                    "resolved": False,
                    "search_key": "",
                    "msg": ("" if i % 2 == 0 else "msg-" + str(i)),
                    "err": ("" if i % 2 != 0 else "err-" + str(i)),
                }


def test_aggregated_reporter_multi(out_path):
    o_path = out_path("csv")
    o_path.unlink()
    r = AggregatedReporter(output_path=o_path, queue=multiprocessing.Queue())
    r.append_log(
        container_type="session",
        label="test",
        container_id="1",
        resolved=False,
        search_key="",
    )
    assert not Path(o_path).exists()
    proc = multiprocessing.Process(target=r.worker)
    proc.start()
    r.queue.put("END")
    proc.join()
    assert os.stat(o_path).st_size
    with open(o_path, "r") as fp:
        lines = fp.readlines()
        assert (
            lines[0]
            == "container_type,container_id,label,err,msg,resolved,search_key\n"
        )
        assert lines[1] == "session,1,test,,,False,\n"
