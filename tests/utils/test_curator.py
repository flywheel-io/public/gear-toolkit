import copy
import sys
from pathlib import Path
from unittest.mock import MagicMock

import flywheel
import pytest

from flywheel_gear_toolkit.context.context import GearToolkitContext
from flywheel_gear_toolkit.utils.curator import (
    FileCurator,
    HierarchyCurator,
    _load_curator,
    get_curator,
)

ASSETS_DIR = Path(__file__).parents[1] / "data"


def test_get_curator():
    gtkc = None
    curator_path = ASSETS_DIR / "dummy_curator.py"
    curator = get_curator(gtkc, curator_path, extra_arg="Test")
    assert curator.context == gtkc
    assert curator.extra_arg == "Test"
    assert str(type(curator)) == "<class 'dummy_curator.Curator'>"
    # Default configuration options
    assert curator.config.depth_first is True
    assert curator.config.reload is True
    assert curator.config.stop_level is None
    assert curator.config.callback is None


def test_load_curator_returns_module():
    # Path as pathlib.Path
    sys.modules.pop("dummy_curator", None)
    assert "dummy_curator" not in sys.modules.keys()
    mod = _load_curator(ASSETS_DIR / "dummy_curator.py")
    assert mod.filename == str(ASSETS_DIR / "dummy_curator.py")
    assert "dummy_curator" in sys.modules.keys()

    # Path as string
    sys.modules.pop("dummy_curator")
    assert "dummy_curator" not in sys.modules.keys()
    mod = _load_curator(str(ASSETS_DIR / "dummy_curator.py"))
    assert mod.filename == str(ASSETS_DIR / "dummy_curator.py")
    assert "dummy_curator" in sys.modules.keys()

    # Returns None if path is corrupted
    mod = _load_curator(str(ASSETS_DIR / "doesnotexist.py"))
    assert mod is None


@pytest.mark.parametrize("cls", [HierarchyCurator, FileCurator])
@pytest.mark.parametrize("val", [True, False])
def test_curator_init(cls, val, make_concrete):
    make_concrete(cls)
    if cls.__name__ == "Curator":
        cc = cls(depth_first=val, write_report=(not val))
        assert cc.depth_first == val
    else:
        cc = cls(write_report=(not val))

    assert cc.write_report == (not val)
    for attr in [
        "optional_requirements",
        "input_file_one",
        "input_file_two",
        "input_file_three",
        "context",
        "client",
    ]:
        if hasattr(cc, attr):
            assert getattr(cc, attr) is None


class Curate(FileCurator):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.my_test = "my" + self.test

    def curate_file(self, file):
        pass


def test_arbitrary_kwargs():
    x = Curate(test="asdf", client="test")

    assert x.test == "asdf"
    assert x.client == "test"
    assert x.my_test == "myasdf"


@pytest.mark.parametrize(
    "container",
    [
        flywheel.Project,
        flywheel.Subject,
        flywheel.Session,
        flywheel.Acquisition,
        flywheel.AnalysisOutput,
        flywheel.FileEntry,
    ],
)
def test_hierarchy_curator_curate_container_calls_hierarchy(
    container, mocker, make_concrete
):
    my_container = container()
    make_concrete(HierarchyCurator)
    curator = HierarchyCurator()

    validate_mock = MagicMock()
    curate_mock = MagicMock()
    # Can't use patch because of my __setattr__ hack :(((
    if hasattr(my_container, "container_type"):
        object.__setattr__(
            curator, f"validate_{container.container_type}", validate_mock
        )
        object.__setattr__(curator, f"curate_{container.container_type}", curate_mock)
    else:
        object.__setattr__(curator, "validate_file", validate_mock)
        object.__setattr__(curator, "curate_file", curate_mock)

    curator.validate_container(container)
    curator.curate_container(container)

    curate_mock.assert_called_once_with(container)
    validate_mock.assert_called_once_with(container)


def test_file_curator_curate_container_calls_hierarchy(mocker, make_concrete):
    make_concrete(FileCurator)
    container = {}
    curator = FileCurator()
    validate_mock = mocker.patch.object(curator, "validate_file")
    curate_mock = mocker.patch.object(curator, "curate_file")

    curator.curate_container(container)
    validate_mock.assert_called_once_with(container)
    curate_mock.assert_called_once_with(container)


def test_client_is_available_as_curator_attribute():
    context = MagicMock(spec=GearToolkitContext)
    curator = HierarchyCurator(context=context)
    assert curator.client is not None


class Curator(HierarchyCurator):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


def test_deep_copy_keeps_shared_attrs():
    context = MagicMock(spec=GearToolkitContext)
    x = Curator(context=context)
    x.data = {"test2": "test1"}
    y = copy.deepcopy(x)
    y.data = {"test1": "test2"}
    y.context.my_test = "test2"
    # Data attribute deep copied
    assert x.data == {"test2": "test1"}
    assert y.data == {"test1": "test2"}
    # Other attributes shallow copied
    assert x.context.my_test == "test2"
    assert y.context.my_test == "test2"
