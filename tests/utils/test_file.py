import os
import tempfile
from pathlib import Path

import pytest

from flywheel_gear_toolkit.utils.file import (
    File,
    is_valid,
    sanitize_filename,
    sanitize_name_general,
)


def test_file_from_config():
    config_def = {
        "hierarchy": {"id": "62449d29ca89ead7f8e16559", "type": "acquisition"},
        "object": {
            "type": "dicom",
            "mimetype": "application/zip",
            "modality": "MR",
            "classification": {},
            "tags": ["file-metadata-importer"],
            "info": {"header": {"dicom": {"AcquisitionDate": "20151201"}}},
            "size": 6476607,
            "zip_member_count": None,
            "version": 9,
            "file_id": "62449d2b9afa19f82ae161ad",
            "origin": {"type": "user", "id": "naterichman@flywheel.io"},
        },
        "location": {
            "path": "/flywheel/v0/input/dicom/301 - MPRAGE.dicom.zip",
            "name": "301 - MPRAGE.dicom.zip",
        },
        "base": "file",
    }

    file = File.from_config(config_def)
    assert file.name == "301 - MPRAGE.dicom.zip"
    assert file.parent_type == "acquisition"
    assert file.modality == "MR"
    assert file.fw_type == "dicom"
    assert file.mimetype == "application/zip"
    assert file.classification == {}
    assert file.tags == ["file-metadata-importer"]
    assert file.info == {"header": {"dicom": {"AcquisitionDate": "20151201"}}}
    assert file.local_path == "/flywheel/v0/input/dicom/301 - MPRAGE.dicom.zip"
    assert file.parents == {}
    assert file.zip_member_count is None
    assert file.version == 9
    assert file.file_id == "62449d2b9afa19f82ae161ad"
    assert file.size == 6476607


def test_file_from_sdk():
    sdk_file = {
        "classification": {},
        "created": None,
        "deid_log_id": None,
        "file_id": "62449d2b9afa19f82ae161ad",
        "hash": "a58e29cee320d1590988f3f",
        "id": "5361b35e-37a1-4bef-b645-3c86eab3b411",
        "info": {"header": {"dicom": {"AcquisitionDate": "20151201"}}},
        "info_exists": True,
        "mimetype": "application/zip",
        "modality": None,
        "modified": None,
        "name": "301 - MPRAGE.dicom.zip",
        "origin": {
            "id": "62a37f7bfd62db3d286c2279",
            "method": None,
            "name": None,
            "type": "job",
            "via": None,
        },
        "parent_ref": {"id": "62449d29ca89ead7f8e16559", "type": "acquisition"},
        "parents": {
            "acquisition": "62449d29ca89ead7f8e16559",
            "analysis": None,
            "group": "nate",
            "project": "620ebb7564c650c00c7887f9",
            "session": "62448de58048c54dbc3a04dd",
            "subject": "62446abce6982a2eec3a0386",
        },
        "provider_id": "6047c661d4a7303a0bfe09d3",
        "replaced": None,
        "size": 23803002,
        "tags": ["dicom-fixer", "file-metadata-importer"],
        "type": "dicom",
        "version": 9,
        "zip_member_count": 170,
    }
    file = File.from_sdk(sdk_file)
    assert file.name == "301 - MPRAGE.dicom.zip"
    assert file.parent_type == "acquisition"
    assert file.modality is None
    assert file.fw_type == "dicom"
    assert file.mimetype == "application/zip"
    assert file.classification == {}
    assert file.tags == ["dicom-fixer", "file-metadata-importer"]
    assert file.info == {"header": {"dicom": {"AcquisitionDate": "20151201"}}}
    assert file.local_path is None
    assert file.parents == {
        "acquisition": "62449d29ca89ead7f8e16559",
        "analysis": None,
        "group": "nate",
        "project": "620ebb7564c650c00c7887f9",
        "session": "62448de58048c54dbc3a04dd",
        "subject": "62446abce6982a2eec3a0386",
    }
    assert file.zip_member_count == 170
    assert file.version == 9
    assert file.file_id == "62449d2b9afa19f82ae161ad"
    assert file.size == 23803002


@pytest.fixture
def create_test_files():
    with tempfile.TemporaryDirectory() as tmp_folder:
        out_dir = Path(tmp_folder, "tmp")
        out_dir.mkdir(parents=True, exist_ok=True)

        file = Path(out_dir, "test_file.ext1.ext2")
        file.touch()

        yield file


@pytest.fixture
def donot_create_test_files():
    with tempfile.TemporaryDirectory() as tmp_folder:
        out_dir = Path(tmp_folder, "tmp")
        out_dir.mkdir(parents=True, exist_ok=True)
        file = Path(out_dir, "non_test_file.ext1.ext2")
        if file.exists():
            os.remove(file)

        yield file


def test_exists_present_and_expected(create_test_files):
    file = create_test_files
    assert is_valid(file, is_expected=True, exception_on_error=False) is True


def test_exists_present_and_unexpected_noexcept(create_test_files):
    file = create_test_files
    assert is_valid(file, is_expected=False, exception_on_error=False) is False


def test_exists_present_and_unexpected_except(create_test_files):
    file = create_test_files
    with pytest.raises(Exception):
        assert is_valid(file, is_expected=False, exception_on_error=True) is False


def test_exists_absent_and_expected_noexcept(donot_create_test_files):
    file = donot_create_test_files
    assert is_valid(file, is_expected=True, exception_on_error=False) is False


def test_exists_absent_and_expected_except(donot_create_test_files):
    file = donot_create_test_files
    with pytest.raises(Exception):
        is_valid(file, is_expected=True, exception_on_error=True)


def test_exists_absent_and_unexpected(donot_create_test_files):
    file = donot_create_test_files
    assert is_valid(file, is_expected=False, exception_on_error=False) is True


def test_exists_single_ext_correct(create_test_files):
    file = create_test_files
    assert is_valid(file, is_expected=True, ext=".ext2") is True


def test_exists_double_ext_correct(create_test_files):
    file = create_test_files
    assert is_valid(file, is_expected=True, ext=".ext1.ext2") is True


def test_exists_long_ext_exception(create_test_files):
    file = create_test_files
    with pytest.raises(Exception):
        is_valid(file, is_expected=True, ext="extension.longer.than.file")


def test_exists_ext_incorrect_noexcept(create_test_files):
    file = create_test_files
    assert (
        is_valid(file, is_expected=True, ext=".bad", exception_on_error=False) is True
    )


def test_exists_ext_incorrect_except(create_test_files):
    file = create_test_files
    with pytest.raises(Exception):
        assert (
            is_valid(file, is_expected=True, ext=".bad", exception_on_error=True)
            is True
        )


@pytest.mark.parametrize(
    "input_name, expected",
    [
        ("t1*.nii.gz", "t1star.nii.gz"),
        ("0001_test_<>_ran+d:o\m.nii.gz", "0001_test___ran+d_o_m.nii.gz"),
    ],
)
def test_sanitize_filename(input_name, expected):
    assert sanitize_filename(input_name) == expected


@pytest.mark.parametrize(
    "input_name, rplc_str, whtspc, expected",
    [
        ("t1*.nii.gz", "", False, "t1star.nii.gz"),
        ("0001_test_<>_ran+d:o\m.nii.gz", "", False, "0001_test___rand_o_m.nii.gz"),
        ("? 55 - fav_acq", "", False, "_55-fav_acq"),
        ("? 56 - fav_acq", "_", False, "__56_-_fav_acq"),
        ("W h i t e s p a c e", "", True, "W h i t e s p a c e"),
        ("W h i t e s p a c e", "", False, "Whitespace"),
    ],
)
def test_sanitize_name_general(input_name, rplc_str, whtspc, expected):
    result = sanitize_name_general(input_name, rplc_str, whtspc)
    assert result == expected
