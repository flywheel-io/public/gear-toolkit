"""Unit test for installing Freesurfer file.
"""

import logging
import os
import subprocess
from pathlib import Path
from unittest.mock import Mock, patch

import pytest

from flywheel_gear_toolkit.licenses.freesurfer import (
    find_license_info,
    get_fs_license_path,
    install_freesurfer_license,
    isolate_key_name,
    write_license_info,
)

# fake freesurfer license with spaces for \n
FS_STR = "name@flywheel.io 93241 *X9YoVve9t72.  XSslwsL42lwtv"

FAKE_PARENTS = {"parents": {"project": "5db0759469d4f3001f16e9c1"}}

FAKE_PROJECT = {"info": {"FREESURFER_LICENSE": FS_STR}}

FAKE_DESTINATION = {"type": "analysis", "id": "5e1dc6954d3a210022725d0f"}


def test_fs_license_in_input_works(tmp_path):
    """Freesurfer license string as an input file"""

    with patch("flywheel.gear_context.GearContext") as mock_context:
        fs_license_dir = Path(tmp_path) / "opt/freesurfer"
        fs_license_dir.mkdir(parents=True)
        fs_license = fs_license_dir / "license.txt"

        input_dir = Path(tmp_path) / "input"
        input_dir.mkdir(parents=True)
        input_license = input_dir / "license.txt"

        # install fake license there
        with open(input_license, "w") as ilp:
            ilp.write("\n".join(FS_STR.split()))

        mock_context.get_input_path = Mock(return_value=input_license)

        install_freesurfer_license(mock_context, fs_license)

        with open(fs_license) as flp:
            whole_file = flp.read()

        assert whole_file == "\n".join(FS_STR.split())


def test_fs_license_in_input_name_different_detected(tmp_path, caplog):
    """Freesurfer license string as an input file"""

    caplog.set_level(logging.DEBUG)

    with patch("flywheel.gear_context.GearContext") as mock_context:
        fs_license_dir = Path(tmp_path) / "opt/freesurfer"
        fs_license_dir.mkdir(parents=True)
        fs_license = fs_license_dir / "horsepucky.txt"

        input_dir = Path(tmp_path) / "input"
        input_dir.mkdir(parents=True)
        input_license = input_dir / "license.txt"

        # install fake license there
        with open(input_license, "w") as ilp:
            ilp.write("\n".join(FS_STR.split()))

        mock_context.get_input_path = Mock(return_value=input_license)

        install_freesurfer_license(mock_context, fs_license)

        assert (
                "Using input file for FreeSurfer license information."
                in caplog.records[2].message
        )


def test_fs_path_does_not_exist_detected(tmp_path, caplog):
    """Freesurfer license string as an input file"""

    caplog.set_level(logging.DEBUG)

    with patch("flywheel.gear_context.GearContext") as mock_context:
        fs_license_dir = Path(tmp_path) / "opt/freesurfer"
        # fs_license_dir.mkdir(parents=True)
        fs_license = fs_license_dir / "license.txt"

        input_dir = Path(tmp_path) / "input"
        input_dir.mkdir(parents=True)
        input_license = input_dir / "license.txt"

        # install fake license there
        with open(input_license, "w") as ilp:
            ilp.write("\n".join(FS_STR.split()))

        mock_context.get_input_path = Mock(return_value=input_license)

        install_freesurfer_license(mock_context, fs_license)

        assert (
                "Using input file for FreeSurfer license information."
                in caplog.records[2].message
        )


def test_fs_license_in_context_works(tmp_path, caplog):
    """Freesurfer license string in gear context"""

    caplog.set_level(logging.DEBUG)

    with patch("flywheel.gear_context.GearContext") as mock_context:
        fs_license_dir = Path(tmp_path) / "opt/freesurfer"
        fs_license = fs_license_dir / "license.txt"

        # pretend no input file was provided
        mock_context.get_input_path = Mock(return_value=None)

        # fake freesurfer license of the proper form
        mock_context.config = {}
        with patch.dict(mock_context.config, {"freesurfer_license_key": FS_STR}):
            install_freesurfer_license(mock_context, fs_license)

            with open(fs_license) as flp:
                whole_file = flp.read()

            assert whole_file == "\n".join(FS_STR.split())
            assert "Created directory" in caplog.records[3].message


def test_fs_license_in_info_works(tmp_path, caplog):
    """Freesurfer license string in project info metadata"""

    caplog.set_level(logging.DEBUG)

    with patch("flywheel.gear_context.GearContext") as mock_context:
        fs_license_path = Path(tmp_path) / "license.txt"

        # pretend no input file was provided
        mock_context.get_input_path = Mock(return_value=None)

        # license is not in (empty) config
        mock_context.config = {}

        mock_context.destination = FAKE_DESTINATION

        with patch("flywheel.client.Client") as mock_client:
            mock_context.client = mock_client

            mock_client.get_analysis = Mock(return_value=FAKE_PARENTS)

            mock_client.get_project = Mock(return_value=FAKE_PROJECT)

            install_freesurfer_license(mock_context, fs_license_path)

            with open(fs_license_path) as flp:
                whole_file = flp.read()

            assert whole_file == "\n".join(FS_STR.split())


def test_fs_license_not_found_exception(caplog):
    """Freesurfer license string not found anywhere"""

    caplog.set_level(logging.DEBUG)

    with patch("flywheel.gear_context.GearContext") as mock_context:
        # a path that does not exist
        fs_license_path = Path("/opt/freesurfer/license.txt")

        # pretend no input file was provided
        mock_context.get_input_path = Mock(return_value=None)

        # license is not in (empty) config
        mock_context.config = {}

        mock_context.destination = FAKE_DESTINATION

        with patch("flywheel.client.Client") as mock_client:
            mock_context.client = mock_client

            mock_client.get_analysis = Mock(return_value=FAKE_PARENTS)

            # no license in project info either
            mock_client.get_project = Mock(return_value={"info": {"not_here": None}})

            with pytest.raises(FileNotFoundError) as excinfo:
                install_freesurfer_license(mock_context, fs_license_path)

            assert "Looking for Freesurfer license" in caplog.records[1].message
            assert "Could not find FreeSurfer license" in str(excinfo.value)
            assert "/opt/freesurfer/license.txt" in str(excinfo.value)


def test_install_freesurfer_license_no_path(tmp_path, monkeypatch):
    monkeypatch.setenv("FREESURFER_HOME", str(tmp_path / "freesurfer_home"))
    fs_license_dir = Path(os.getenv("FREESURFER_HOME"))
    fs_license_dir.mkdir(parents=True, exist_ok=True)
    fs_license = fs_license_dir / "license.txt"

    with patch("flywheel.gear_context.GearContext") as mock_context:
        input_dir = Path(tmp_path) / "input"
        input_dir.mkdir(parents=True)
        input_license = input_dir / "license.txt"

        # install fake license there
        with open(input_license, "w") as ilp:
            ilp.write("\n".join(FS_STR.split()))

        mock_context.get_input_path = Mock(return_value=input_license)

        install_freesurfer_license(mock_context)

        with open(fs_license) as flp:
            whole_file = flp.read()

        assert whole_file == "\n".join(FS_STR.split())


def test_get_fs_license_path_with_env_variable(env_setup):
    expected_path = Path(
        f"{Path(__file__).parent.parent}/data/opt/freesurfer/license.txt"
    )
    assert get_fs_license_path() == expected_path


@patch("subprocess.check_output")
def test_get_fs_license_path_without_env_variable(mock_check_output):
    os.environ["FREESURFER_HOME"] = ""
    supplied_path = "/path/to/freesurfer/recon-all"
    mock_check_output.return_value = supplied_path
    expected_path = Path("/path/to/freesurfer/license.txt")
    resultant_path = get_fs_license_path()
    assert resultant_path == expected_path


@patch("os.getenv", return_value=False)
@patch("subprocess.check_output")
def test_get_fs_license_path_command_error(mock_check_output, mock_getenv):
    mock_check_output.side_effect = subprocess.CalledProcessError(
        1, "which freesurfer", "Command not found"
    )
    with pytest.raises(subprocess.CalledProcessError):
        get_fs_license_path()


def test_find_license_info_with_config_key(mock_context):
    mock_context.config = {"freesurfer_license_key": "LICENSE_KEY"}
    mock_context.get_input_path.return_value = None
    license_info = find_license_info(mock_context)
    assert license_info == "LICENSE_KEY"


def test_find_license_info_with_project_info(mock_context):
    mock_context.config.get.side_effect = lambda key: {
        "freesurfer_license_key": None
    }.get(key)
    mock_context.get_input_path.return_value = None
    mock_context.client.get_analysis.return_value = {
        "parents": {"project": "PROJECT_ID"}
    }
    mock_context.client.get_project.return_value = {
        "info": {"FREESURFER_LICENSE": "LICENSE_KEY"}
    }
    license_info = find_license_info(mock_context)
    assert license_info == "LICENSE_KEY"


def test_write_license_info(tmp_path, caplog):
    fs_license_path = tmp_path / "license.txt"
    license_info = "Bippity boppity boo"
    caplog.set_level(logging.DEBUG)
    write_license_info(fs_license_path, license_info)

    assert fs_license_path.exists()
    assert fs_license_path.read_text() == license_info
    assert f"Wrote license {license_info}" in caplog.records[0].message
    assert not any("Created directory" in rec.message for rec in caplog.records)


@pytest.mark.parametrize("context_config, expected",
                         [({"freesurfer_license_key": "1234"},
                           "freesurfer_license_key"),
                          ({"FREESURFER_LICENSE": "abcd"}, "FREESURFER_LICENSE"),
                          ({"freesurfer_license": "my_license"}, "freesurfer_license"),
                          ({}, None),
                          ({'bogus_key': 'zxcv'}, None)
                          ])
def test_isolate_key_name_returns_value(context_config, expected):
    mock_context = type('Context', (), {'config': context_config})

    value = isolate_key_name(mock_context)
    assert value == expected
