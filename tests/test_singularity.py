from pathlib import Path
from unittest.mock import patch

import pytest

from flywheel_gear_toolkit.hpc.singularity import (
    check_for_singularity,
    check_writable_dir,
    log_singularity_details,
    mount_file,
    mount_gear_home_to_tmp,
    start_singularity,
    unlink_gear_mounts,
)
from flywheel_gear_toolkit.testing.hierarchy import MockFileEntry


@pytest.mark.parametrize(
    "input_env, expected_result",
    [("SINGULARITY_NAME", True), ("No_special_env", False)],
)
def test_check_for_singularity(input_env, expected_result):
    with patch.dict(
        "flywheel_gear_toolkit.hpc.singularity.os.environ",
        {input_env: "example"},
        clear=True,
    ):
        result = check_for_singularity()
    assert result == expected_result


@patch("flywheel_gear_toolkit.hpc.singularity.os.access", return_value=True)
@patch("flywheel_gear_toolkit.hpc.singularity.os.W_OK", return_value=True)
def test_check_writable_dir_returns_writable_dir(mock_read, mock_access, caplog):
    access = check_writable_dir("scribbles/on/chalkboard")
    assert access
    assert len(caplog.records) == 1


def test_check_writable_dir_returns_unwritable_to_log(caplog):
    access = check_writable_dir("chalkboard")
    assert "NOT writable" in caplog.messages[0]
    assert not access


@patch("flywheel_gear_toolkit.hpc.singularity.os.getuid", return_value="100")
def test_log_singularity_details(
    mock_uid,
    caplog,
):
    caplog.set_level("DEBUG")
    with patch.dict(
        "flywheel_gear_toolkit.hpc.singularity.os.environ",
        {"SINGULARITY_NAME": "ralphie"},
        clear=True,
    ):
        log_singularity_details()
    assert len(caplog.messages) == 2


@pytest.mark.parametrize("exists, expected_num_of_mkdirs", [(True, 0), (False, 1)])
@patch("flywheel_gear_toolkit.hpc.singularity.Path")
def test_mount_file(mock_Path, exists, expected_num_of_mkdirs):
    mock_orig_path = "pretend/I/am/here"
    mock_new_path = Path("me/too")
    with patch.object(Path, "mkdir") as new_dir:
        with patch.object(Path, "exists") as new_path:
            new_path.return_value = exists
            mount_file(mock_orig_path, mock_new_path, "string_bean")
    assert new_dir.call_count == expected_num_of_mkdirs


@patch("flywheel_gear_toolkit.hpc.singularity.os.chdir")
@patch("flywheel_gear_toolkit.hpc.singularity.mount_file")
@patch("flywheel_gear_toolkit.hpc.singularity.tempfile.mkdtemp")
@patch("flywheel_gear_toolkit.hpc.singularity.Path")
def test_mount_gear_home_to_tmp(mock_Path, mock_temp, mock_mount, mock_chdir):
    mock_gear = "stick_shift"
    mock_wrt_dir = Path("write/here")
    mock_Path.return_value.glob.return_value = [
        MockFileEntry(parent="Michael", name="path1"),
        MockFileEntry(parent="Nicolas", name="path2"),
        MockFileEntry(parent="Andy", name="gear_environ.json"),
    ]

    with patch.object(Path, "mkdir") as new_dir:
        mount_gear_home_to_tmp(mock_gear, mock_wrt_dir)
    assert mock_mount.called_once
    assert mock_Path.return_value.symlink_to.call_count == 3
    assert mock_chdir.called


@patch("flywheel_gear_toolkit.hpc.singularity.mount_gear_home_to_tmp")
@patch("flywheel_gear_toolkit.hpc.singularity.check_writable_dir", return_value=True)
@patch("flywheel_gear_toolkit.hpc.singularity.log_singularity_details")
def test_start_singularity(mock_log, mock_wrtbl, mock_mount):
    mock_gear = "needs_engine"
    mock_wrt_dir = Path("write/here")
    start_singularity(mock_gear, mock_wrt_dir, True)
    assert mock_log.called_once
    assert mock_wrtbl.call_count == 1
    assert mock_mount.called_once


@patch("flywheel_gear_toolkit.hpc.singularity.mount_gear_home_to_tmp")
@patch("flywheel_gear_toolkit.hpc.singularity.check_writable_dir", return_value=False)
@patch("flywheel_gear_toolkit.hpc.singularity.log_singularity_details")
def test_start_singularity_fails(mock_log, mock_wrtbl, mock_mount, caplog):
    mock_gear = "needs_engine"
    mock_wrt_dir = Path("write/here")
    start_singularity(mock_gear, mock_wrt_dir, True)
    assert mock_log.called_once
    assert mock_wrtbl.call_count == 2
    assert "error" in caplog.text.lower()


@patch("flywheel_gear_toolkit.hpc.singularity.shutil.rmtree")
@patch("flywheel_gear_toolkit.hpc.singularity.os.unlink")
@patch("flywheel_gear_toolkit.hpc.singularity.os.path.islink", return_value=True)
@patch("flywheel_gear_toolkit.hpc.singularity.glob")
@patch("flywheel_gear_toolkit.hpc.singularity.Path")
def test_unlink_gear_mounts(
    mock_Path, mock_glob, mock_link, mock_unlink, mock_rm, caplog
):
    mock_Path.return_value = "temp_gear_dir"
    mock_glob.return_value = ["item1", "item2", "item3"]
    caplog.set_level("DEBUG")
    unlink_gear_mounts("pretend_to_write")
    assert len(caplog.records) == len(mock_glob.return_value) + 1
    assert mock_unlink.called_once
