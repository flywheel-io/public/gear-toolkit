from pathlib import Path
from unittest.mock import patch

import pytest

from flywheel_gear_toolkit.hpc.freesurfer_utils import (
    check_subjects_dir_from_zip_file_path,
    fetch_previous_results_zip_file,
    set_FS_templates,
    set_templateflow_dir,
    setup_freesurfer_for_singularity,
)


@pytest.mark.parametrize("file_exists, expected_result", [(True, 0), (False, 3)])
@patch("flywheel_gear_toolkit.hpc.freesurfer_utils.unzip_archive")
@patch("flywheel_gear_toolkit.hpc.freesurfer_utils.Path")
def test_check_subjects_dir_from_zip_file_path(
    mock_Path, mock_unzip, file_exists, expected_result
):
    mock_Path.return_value.glob.return_value = [
        Path("prev/FS/result1"),
        Path("prev/FS/result45"),
    ]
    mock_Path.return_value.exists.return_value = file_exists
    with patch.dict(
        "flywheel_gear_toolkit.hpc.freesurfer_utils.os.environ",
        {"SUBJECTS_DIR": "a/dir"},
        clear=True,
    ):
        with patch.object(Path, "glob") as mock_unzip_dir:
            mock_unzip_dir.return_value = [
                Path("twenty"),
                Path("thirty"),
                Path("forty"),
            ]
            with patch.object(Path, "mkdir"):
                with patch.object(Path, "rename") as mock_rename:
                    check_subjects_dir_from_zip_file_path(
                        Path("main", "writable", "dir")
                    )
    assert mock_unzip.called_once
    assert mock_rename.call_count == expected_result


@patch("flywheel_gear_toolkit.hpc.freesurfer_utils.unzip_archive")
@patch("flywheel_gear_toolkit.hpc.freesurfer_utils.Path")
def test_fetch_previous_results_zip_file(mock_Path, mock_unzip):
    mock_Path.return_value.glob.return_value = [
        Path("prev/result1"),
        Path("prev/result45"),
    ]
    with patch.object(Path, "glob") as mock_unzip_dir:
        mock_unzip_dir.return_value = [Path("ten"), Path("twelve")]
        with patch.object(Path, "mkdir"):
            with patch.object(Path, "rename") as mock_rename:
                fetch_previous_results_zip_file(
                    Path("rosetta", "stone"),
                    Path("end", "result", "please"),
                    "Bahamas",
                    Path("deja/vous"),
                )
    assert mock_unzip.called_once
    assert mock_rename.call_count == len(mock_unzip_dir.return_value)


@patch("flywheel_gear_toolkit.hpc.freesurfer_utils.singularity.mount_file")
def test_set_FS_templates_finds_three_templates(mock_mount):
    with patch.dict(
        "flywheel_gear_toolkit.hpc.freesurfer_utils.os.environ",
        {"SUBJECTS_DIR": "a/dir"},
        clear=True,
    ):
        set_FS_templates("orig/dir")
    assert mock_mount.call_count == 3


@patch("flywheel_gear_toolkit.hpc.freesurfer_utils.shutil.copytree")
@patch("flywheel_gear_toolkit.hpc.freesurfer_utils.Path")
def test_set_templateflow_dir_passes(mock_Path, mock_copy):
    """Test that the TemplateFlow directory can be set."""
    mock_Path.return_value.glob.return_value = [Path("temp1"), Path("temp2")]
    with patch.object(Path, "mkdir"):
        with patch.dict(
            "flywheel_gear_toolkit.hpc.freesurfer_utils.os.environ",
            {
                "SINGULARITYENV_TEMPLATEFLOW_HOME": "a/dir",
                "TEMPLATEFLOW_HOME": "a/different/dir",
            },
            clear=True,
        ):
            set_templateflow_dir(Path("my_writable_dir"))
    assert mock_copy.call_count == len(mock_Path.return_value.glob.return_value)


@patch(
    "flywheel_gear_toolkit.hpc.freesurfer_utils.check_subjects_dir_from_zip_file_path"
)
@patch("flywheel_gear_toolkit.hpc.freesurfer_utils.set_FS_templates")
def test_setup_freesurfer_for_singularity_passes(mock_templates, mock_check_dirs):
    """Test that the overarching 'run' method works to set the templates and fill
    in subject directory contents."""
    with patch.dict(
        "flywheel_gear_toolkit.hpc.freesurfer_utils.os.environ",
        {"SUBJECTS_DIR": "a/dir"},
        clear=True,
    ):
        setup_freesurfer_for_singularity(Path("scribble/scribble"))
    assert mock_templates.called_once
    assert mock_check_dirs.called_once
