import argparse
import json
import os
import sys
import tempfile
from pathlib import Path
from unittest import mock

import flywheel
import pytest
from flywheel_bids import export_bids

import flywheel_gear_toolkit as gtk
from flywheel_gear_toolkit.context.context import GearToolkitContext
from flywheel_gear_toolkit.context.context import convert_config_type


def test_convert_config_type():
    # test no type
    input_str = "4"
    output_val = convert_config_type(input_str)
    assert output_val == input_str
    assert isinstance(output_val, str)

    # test str, string
    for input_str in ["4:string", "4:str"]:
        output_val = convert_config_type(input_str)
        assert output_val == "4"
        assert isinstance(output_val, str)

    # test booleans
    for input_str in [
        "false:boolean",
        "False:boolean",
        "FALSE:bool",
        "true:bool",
        "TRUE:boolean",
        "true:bool",
    ]:
        output_val = convert_config_type(input_str)
        assert isinstance(output_val, bool)
        if "false" in input_str.lower():
            assert output_val is False
        elif "true" in input_str.lower():
            assert output_val is True

    # test integers
    for input_str in ["4:number", "4:integer", "4:int"]:
        output_val = convert_config_type(input_str)
        assert output_val == 4
        assert isinstance(output_val, int)

    # test floats
    for input_str in ["4:float", "4.0:number"]:
        output_val = convert_config_type(input_str)
        assert output_val == 4
        assert isinstance(output_val, float)

    # test input_str not a string
    with pytest.raises(ValueError) as err:
        input_str = 4
        assert convert_config_type(input_str)
    assert str(err.value) == "input_str 4 is not str"

    # test boolean input_str that is not true or false:
    with pytest.raises(ValueError) as err:
        input_str = "4:boolean"
        assert convert_config_type(input_str)
    assert str(err.value) == "Cannot convert 4 to a boolean"

    # test unknown type_str
    with pytest.raises(ValueError) as err:
        input_str = "4:garbage"
        assert convert_config_type(input_str)
    assert str(err.value) == "Unrecognized type_str: garbage"


def test_context_default_properties(tmpdir):
    with GearToolkitContext(gear_path=tmpdir) as context:
        assert context.config == {}
        assert context.destination == {"id": "aex", "type": "acquisition"}
        assert context.work_dir == tmpdir / "work"
        assert context.output_dir == tmpdir / "output"
        with context.open_output("spam.txt", "a") as appnd:
            appnd.close()


def test_context_inputs(json_file):
    config_path = json_file("config")
    with open(config_path, "r") as fp:
        config_dict = json.load(fp)
    with GearToolkitContext(
        config_path=config_path, tempdir=True
    ) as context:
        # test get input
        assert context.get_input("dicom") == config_dict["inputs"].get("dicom")

        # test valid get_input_path
        assert (
            context.get_input_path("dicom")
            == config_dict["inputs"]["dicom"]["location"]["path"]
        )

        # test None get_input_path
        assert context.get_input_path("spam") is None

        # test invalid get_input_path
        with pytest.raises(ValueError) as err:
            assert context.get_input_path("api-key")
        assert str(err.value) == "The specified input api-key is not a file"

        # test valid get_input_filename
        assert (
            context.get_input_filename("dicom")
            == config_dict["inputs"]["dicom"]["location"]["name"]
        )

        # test None get_input_filename
        assert context.get_input_filename("spam") is None

        # test invalid get_input_filename
        with pytest.raises(ValueError) as err:
            assert context.get_input_filename("api-key")
        assert str(err.value) == "The specified input api-key is not a file"

        # test valid context
        context_val = context.get_context_value("classifications")
        assert context_val == "spam"

        # test undefined context
        context_val = context.get_context_value("doesnt_exist")
        assert context_val is None

        # test invalid context
        with pytest.raises(ValueError) as err:
            assert context.get_context_value("api-key")
        assert str(err.value) == "The specified input api-key is not a context input"

        # test invalid api-key
        assert context.get_input("api-key")["key"] == "my_api_key"
        assert context.client is None
        context.config_json["inputs"].pop("api-key")

        # test no api-key
        with mock.patch("flywheel.client.get_api_key_from_cli", return_value=None):
            assert context.get_input("api-key") is None
            assert context.client is None


def test_context_open_output():
    with GearToolkitContext(tempdir=True) as context:
        context.config_json["inputs"] = {
            "dicom": {
                "base": "file",
                "hierarchy": {"id": "aex", "type": "acquisition"},
                "location": {
                    "name": "T1w_MPR.dcm.zip",
                    "path": context.output_dir / "T1w_MPR.dcm.zip",
                },
            }
        }
        # test invalid open_input
        with pytest.raises(ValueError) as err:
            assert context.open_input("spam")
        assert str(err.value) == "Input spam is not defined in the config.json"

        # test non-existent open_input
        input_path = context.get_input_path("dicom")
        with pytest.raises(FileNotFoundError) as err:
            assert context.open_input("dicom")
        assert str(err.value) == f"Input dicom does not exist at {input_path}"

        # test valid open_input
        Path(input_path).touch()
        with context.open_input("dicom") as rdr:
            rdr.close()


def test_context_init_logging():
    with GearToolkitContext(tempdir=True) as context:
        # test default
        default_config_name, update_config = context.init_logging()
        assert update_config is None
        assert default_config_name == "info"

        # test debug
        debug_file = context.output_dir / "job.log"
        default_config_name, _ = context.init_logging(default_config_name="debug")
        assert default_config_name == "debug"
        assert debug_file.exists()

        # test debug from config
        context.config["debug"] = True
        default_config_name, _ = context.init_logging()
        assert default_config_name == "debug"
        assert debug_file.exists()

        # test debug from config with job id
        context.config["debug"] = True
        context.config_json["job"] = {"id": "1234567890"}
        debug_file = context.output_dir / "job-1234567890.log"
        default_config_name, _ = context.init_logging()
        assert default_config_name == "debug"
        assert debug_file.exists()

        # test manifest.custom.log_config
        context.manifest = {
            "custom": {
                "log_config": {
                    "formatters": {
                        "gear": {"format": "[TEST] {levelname:4s} {message:s}"}
                    }
                }
            }
        }
        _, update_config = context.init_logging()
        assert update_config == context.manifest["custom"]["log_config"]


def test_context_log_config(json_file):
    config_path = json_file("config")
    with GearToolkitContext(
        config_path=config_path, tempdir=True
    ) as context:
        context.log_config()


def test_context_load_download_bids():
    with GearToolkitContext(tempdir=True) as context:
        from flywheel_bids.export_bids import download_bids_dir

        assert context._load_download_bids() == download_bids_dir

    # test ImportError
    with mock.patch.dict("sys.modules", {"flywheel_bids.export_bids": None}):
        with pytest.raises(RuntimeError) as err:
            assert context._load_download_bids()
        assert (
            str(err.value)
            == "Unable to load flywheel-bids package, make sure it is installed!"
        )


def test_context_json_decode():
    with tempfile.TemporaryDirectory() as tempdir:
        dir_path = Path(tempdir)
        text_file_path = dir_path / "spam.txt"
        text_file_path.touch()
        text_file_path.write_text("Lorem ipsum dolor sit amet")
        with pytest.raises(RuntimeError) as err:
            assert GearToolkitContext(
                tempdir=True, config_path=text_file_path
            )
        assert str(err.value) == f"Cannot parse {text_file_path} as JSON."


def test_context_validate_bids_download(mocker):
    # mock client as a dictionary
    mock_acquisition = {
        "session": "mock_session_id",
        "parents": {
            "acquisition": None,
            "analysis": None,
            "group": "mock_group_id",
            "project": "mock_project_id",
            "session": "mock_session_id",
            "subject": "mock_subject_id",
        },
    }
    mock_client = {"aex": mock_acquisition}
    mocker.patch.object(gtk.context.context.GearToolkitContext, "__exit__")

    with GearToolkitContext(tempdir=True) as context:
        context._client = mock_client
        # test valid session command
        (
            parent_id,
            target_dir,
            download_bids_dir,
            kwargs,
        ) = context._validate_bids_download(
            container_type="session",
            target_dir=None,
            sessions=None,
            subjects=["mock_subject_id"],
        )
        expected_target_dir = context.work_dir / "bids"
        assert parent_id == "mock_session_id"
        assert target_dir == expected_target_dir
        assert download_bids_dir == export_bids.download_bids_dir
        assert kwargs == {"subjects": ["mock_subject_id"]}

        # test valid subject command with str target_dir
        (
            parent_id,
            target_dir,
            download_bids_dir,
            kwargs,
        ) = context._validate_bids_download(
            container_type="project",
            target_dir=f"{context.work_dir}/bids",
            sessions=["mock_session_id"],
            subjects=None,
        )
        assert parent_id == "mock_project_id"
        assert target_dir == (context.work_dir / "bids")
        assert kwargs == {"sessions": ["mock_session_id"]}

        # test valid subject command with Path target_dir
        (
            parent_id,
            target_dir,
            download_bids_dir,
            kwargs,
        ) = context._validate_bids_download(
            container_type="project",
            target_dir=context.work_dir / "bids",
            sessions=["mock_session_id"],
            subjects=None,
        )
        assert parent_id == "mock_project_id"
        assert target_dir == (context.work_dir / "bids")
        assert kwargs == {"sessions": ["mock_session_id"]}

        # test bad dir type
        with pytest.raises(TypeError) as err:
            assert context._validate_bids_download(
                container_type="project",
                target_dir=8,
                sessions=["mock_session_id"],
                subjects=None,
            )
        # test no parent id
        with pytest.raises(RuntimeError) as err:
            assert context._validate_bids_download(
                container_type="acquisition",
                target_dir=None,
                sessions=["mock_session_id"],
                subjects=None,
            )


@pytest.mark.parametrize(
    "dest_ref,getter,expected",
    [
        ({"id": "123", "type": "analysis"}, "get_analysis", flywheel.AnalysisOutput()),
        (
            {"id": "123", "type": "acquisition"},
            "get_acquisition",
            flywheel.Acquisition(),
        ),
    ],
)
def test_get_destination_container(dest_ref, getter, expected, sdk_mock):
    context = GearToolkitContext(input_args=[])
    getattr(sdk_mock, getter).return_value = expected
    context._client = sdk_mock
    context.config_json["destination"] = dest_ref

    res = context.get_destination_container()

    assert res == expected


@pytest.mark.parametrize(
    "parent,getter",
    [
        (flywheel.Subject(id="123"), "get_subject"),
        (flywheel.Session(id="123"), "get_session"),
        (flywheel.Acquisition(id="123"), "get_acquisition"),
    ],
)
def test_get_destination_parent_for_analysis(sdk_mock, parent, getter):
    context = GearToolkitContext(input_args=[])
    context.config_json = {}
    context.config_json["destination"] = {"id": "123", "type": "analysis"}
    context.get_destination_container = mock.MagicMock(
        return_value=flywheel.AnalysisOutput(parent=parent.ref())
    )
    getattr(sdk_mock, getter).return_value = parent
    context._client = sdk_mock

    res = context.get_destination_parent()

    assert res == parent


def test_get_destination_parent_for_group_raises():
    context = GearToolkitContext(input_args=[])
    context.get_destination_container = mock.MagicMock(return_value=flywheel.Group())
    with pytest.raises(TypeError) as exc:
        context.get_destination_parent()


@pytest.mark.parametrize(
    "dest,expected",
    [
        (
            flywheel.Acquisition(
                parents={"group": "a", "project": "b", "subject": "c", "session": "d"}
            ),
            ("session", "d"),
        ),
        (
            flywheel.Session(
                parents={"group": "a", "project": "b", "subject": "c", "session": None}
            ),
            ("subject", "c"),
        ),
        (
            flywheel.Subject(
                parents={"group": "a", "project": "b", "subject": None, "session": None}
            ),
            ("project", "b"),
        ),
        (
            flywheel.Project(
                parents={
                    "group": "a",
                    "project": None,
                    "subject": None,
                    "session": None,
                }
            ),
            ("group", "a"),
        ),
    ],
)
def test_get_destination_parent_for_hierarchical_containers(dest, expected, mocker):
    context = GearToolkitContext(input_args=[])
    context.get_destination_container = mock.MagicMock(return_value=dest)
    mocker_get_container_from_ref = mocker.patch.object(
        context, "get_container_from_ref"
    )

    context.get_destination_parent()

    mocker_get_container_from_ref.assert_called_once_with(
        {"type": expected[0], "id": expected[1]}
    )


def test_get_parent_for_analysis(sdk_mock):
    context = GearToolkitContext(input_args=[])
    analysis = flywheel.AnalysisOutput(parent={"type": "session", "id": "anid"})
    getattr(sdk_mock, "get_session").return_value = flywheel.Session(id="anid")
    context._client = sdk_mock

    res = context.get_parent(analysis)

    assert res.ref() == {"type": "session", "id": "anid"}


def test_get_parent_for_file(sdk_mock):
    context = GearToolkitContext(input_args=[])
    file = flywheel.FileEntry()
    file._parent = flywheel.Acquisition(id="anid")

    res = context.get_parent(file)

    assert res.ref() == {"type": "acquisition", "id": "anid"}


@pytest.mark.parametrize(
    "dest_ref,getter,expected",
    [
        ({"id": "123", "type": "analysis"}, "get_analysis", flywheel.AnalysisOutput()),
        (
            {"id": "123", "type": "acquisition"},
            "get_acquisition",
            flywheel.Acquisition(),
        ),
    ],
)
def test_get_destination_container(dest_ref, getter, expected, sdk_mock):
    context = GearToolkitContext(input_args=[])
    getattr(sdk_mock, getter).return_value = expected
    context._client = sdk_mock
    context.config_json["destination"] = dest_ref

    res = context.get_destination_container()

    assert res == expected


@pytest.mark.parametrize(
    "parent,getter",
    [
        (flywheel.Subject(id="123"), "get_subject"),
        (flywheel.Session(id="123"), "get_session"),
        (flywheel.Acquisition(id="123"), "get_acquisition"),
    ],
)
def test_get_destination_parent_for_analysis(sdk_mock, parent, getter):
    context = GearToolkitContext()
    context.config_json = {}
    context.config_json["destination"] = {"id": "123", "type": "analysis"}
    context.get_destination_container = mock.MagicMock(
        return_value=flywheel.AnalysisOutput(parent=parent.ref())
    )
    getattr(sdk_mock, getter).return_value = parent
    context._client = sdk_mock

    res = context.get_destination_parent()

    assert res == parent


def test_get_destination_parent_for_group_raises():
    context = GearToolkitContext()
    context.get_destination_container = mock.MagicMock(return_value=flywheel.Group())
    with pytest.raises(TypeError) as exc:
        context.get_destination_parent()


@pytest.mark.parametrize(
    "dest,expected",
    [
        (
            flywheel.Acquisition(
                parents={"group": "a", "project": "b", "subject": "c", "session": "d"}
            ),
            ("session", "d"),
        ),
        (
            flywheel.Session(
                parents={"group": "a", "project": "b", "subject": "c", "session": None}
            ),
            ("subject", "c"),
        ),
        (
            flywheel.Subject(
                parents={"group": "a", "project": "b", "subject": None, "session": None}
            ),
            ("project", "b"),
        ),
        (
            flywheel.Project(
                parents={
                    "group": "a",
                    "project": None,
                    "subject": None,
                    "session": None,
                }
            ),
            ("group", "a"),
        ),
    ],
)
def test_get_destination_parent_for_hierarchical_containers(dest, expected, mocker):
    context = GearToolkitContext()
    context.get_destination_container = mock.MagicMock(return_value=dest)
    mocker_get_container_from_ref = mocker.patch.object(
        context, "get_container_from_ref"
    )

    context.get_destination_parent()

    mocker_get_container_from_ref.assert_called_once_with(
        {"type": expected[0], "id": expected[1]}
    )


def test_get_parent_for_analysis(sdk_mock):
    context = GearToolkitContext()
    analysis = flywheel.AnalysisOutput(parent={"type": "session", "id": "anid"})
    getattr(sdk_mock, "get_session").return_value = flywheel.Session(id="anid")
    context._client = sdk_mock

    res = context.get_parent(analysis)

    assert res.ref() == {"type": "session", "id": "anid"}


def test_get_parent_for_file(sdk_mock):
    context = GearToolkitContext()
    file = flywheel.FileEntry()
    file._parent = flywheel.Acquisition(id="anid")

    res = context.get_parent(file)

    assert res.ref() == {"type": "acquisition", "id": "anid"}


def test_zip_member_count_get_populated_upon_exit(zips, monkeypatch):
    zip_dir = zips([("test1.zip", 2), ("test2.zip", 100)])

    with gtk.context.context.GearToolkitContext(gear_path=zip_dir) as context:
        monkeypatch.setattr(context, "_out_dir", zip_dir)
        context.config_json = {"destination": {"type": "analysis"}}
    files = context.metadata._metadata["analysis"]["files"]
    assert {"name": "test1.zip", "info": {}, "zip_member_count": 2} in files
    assert {"name": "test2.zip", "info": {}, "zip_member_count": 100} in files


def test_context_write_metadata_when_sys_exit_is_0(tmpdir):
    with pytest.raises(SystemExit) as exc:
        with gtk.context.context.GearToolkitContext(gear_path=tmpdir) as gtk_context:
            gtk_context.update_container_metadata("acquisition", {"key": "value"})
            sys.exit(0)

    with open(tmpdir / "output" / ".metadata.json", "r") as fp:
        data = json.load(fp)
    assert "acquisition" in data


@pytest.mark.parametrize(
    "clean, exp_file_count, exp_dir_count", [(True, 0, 0), (False, 2, 1)]
)
def test_clean_output_on_exit_code_nonzero(
    tmpdir, clean, exp_file_count, exp_dir_count
):
    with pytest.raises(SystemExit):
        with gtk.context.context.GearToolkitContext(
            gear_path=tmpdir, clean_on_error=clean
        ) as context:
            # Generate 2 files in temp output directory
            output_dir = context.output_dir
            open(context.output_dir / "foo.txt", "w").close()
            open(context.output_dir / "bar.txt", "w").close()

            # Generate 1 directory
            os.mkdir(context.output_dir / "peanuts")

            # Assert number of files and number of directories.
            n_files = len(
                [
                    file
                    for file in os.listdir(output_dir)
                    if os.path.isfile(os.path.join(output_dir, file))
                ]
            )
            n_dirs = len(
                [
                    dir_name
                    for dir_name in os.listdir(output_dir)
                    if os.path.isdir(os.path.join(output_dir, dir_name))
                ]
            )
            assert n_files == 2
            assert n_dirs == 1
            # Force SystemExit(1)
            sys.exit(1)

        # Assert zero files and directories upon exit. If debug, assert original number of files.
        n_files = len(
            [
                file
                for file in os.listdir(output_dir)
                if os.path.isfile(os.path.join(output_dir, file))
            ]
        )
        n_dirs = len(
            [
                dir_name
                for dir_name in os.listdir(output_dir)
                if os.path.isdir(os.path.join(output_dir, dir_name))
            ]
        )
        assert n_files == exp_file_count
        assert n_dirs == exp_dir_count
