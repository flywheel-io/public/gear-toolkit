import logging
import tempfile

import pytest
from test_logging import template_helper_fn

from flywheel_gear_toolkit.interfaces.command_line import (
    build_command_list,
    exec_command,
)

log = logging.getLogger(__name__)


def test_build_command_list():
    # Multiple ASSERT statements can be made OR...

    # test single-character no value
    command = ["ls"]
    params = {"l": True, "a": True, "h": True}
    command1 = build_command_list(command, params)
    result1 = ["ls", "-l", "-a", "-h"]
    assert command1 == result1

    # test with single-character with value
    command = ["head"]
    params = {"n": "5"}
    command2 = build_command_list(command, params)
    result2 = ["head", "-n", "5"]
    assert command2 == result2

    # test with boolean value
    command = ["head"]
    params = {"n": "5", "i": True, "Bool": True}
    command2 = build_command_list(command, params)
    result2 = ["head", "-n", "5", "-i", "--Bool"]
    assert command2 == result2

    # test with multi-character no value and with value
    command = ["du"]
    params = {"a": True, "human-readable": True, "max-depth": 3}
    command3 = build_command_list(command, params)
    result3 = ["du", "-a", "--human-readable", "--max-depth=3"]
    assert command3 == result3

    # test without keys
    command = ["nonsense"]
    params = {"File1": "this.txt", "File2": "that.txt"}
    command4 = build_command_list(command, params, include_keys=False)
    result4 = ["nonsense", "this.txt", "that.txt"]
    assert command4 == result4


def test_exec_command(caplog):
    template_helper_fn()
    command = ["ls"]
    params = {"l": True, "a": True, "h": True}
    command1 = build_command_list(command, params)
    exec_command(command1)
    for record in caplog.records:
        assert record.levelname not in ("CRITICAL", "ERROR")


def test_exec_command_fail(caplog):
    template_helper_fn()
    command = ["ls"]
    params = {"ldfs": True, "a": True, "h": True}
    command1 = build_command_list(command, params)

    with pytest.raises(RuntimeError) as err:
        assert exec_command(command1)

        assert caplog.records[3].levelname == "ERROR"

        assert "The following command has failed: " in str(err.value)


def test_param_list_w_invalid_values(caplog):
    template_helper_fn()
    command = ["ls"]
    params = {"l": "", "a": None, "h": True}
    command = build_command_list(command, params)

    exp_messages = [
        'Removing parameter with empty value for key "l".',
        'Removing parameter with empty value for key "a".',
    ]

    exp_levelnames = ["WARNING", "WARNING"]

    assert command == ["ls", "-h"]

    for i in range(len(caplog.records)):
        if exp_messages[i]:
            assert caplog.records[i].message == exp_messages[i]
            assert caplog.records[i].levelname == exp_levelnames[i]


def test_exec_command_dry_run(caplog):
    template_helper_fn()
    command = ["ls"]
    params = {"l": True, "a": True, "h": True}
    command1 = build_command_list(command, params)
    exec_command(command1, dry_run=True)
    for record in caplog.records:
        assert record.levelname not in ("CRITICAL", "ERROR")


def test_exec_command_shell(caplog):
    template_helper_fn()
    command = ["ls"]
    params = {"l": True, "a": True, "h": True}
    command1 = build_command_list(command, params)
    with tempfile.NamedTemporaryFile() as ntf:
        params = {"redirect": ">>", "file": ntf.name}
        command1 = build_command_list(command1, params, include_keys=False)
        exec_command(command1, shell=True)
        for record in caplog.records:
            assert record.levelname not in ("CRITICAL", "ERROR")


def test_exec_command_stdout_msg(caplog):
    template_helper_fn()
    command = ["ls"]
    params = {"l": True, "a": True, "h": True}
    command1 = build_command_list(command, params)
    exec_command(command1, stdout_msg="No StdOut!!!")
    for record in caplog.records:
        assert record.levelname not in ("CRITICAL", "ERROR")


def test_exec_command_cont_output(caplog):
    template_helper_fn()
    command = ["ls"]
    params = {"l": True, "a": True, "h": True}
    command1 = build_command_list(command, params)
    exec_command(command1, cont_output=True)
    for record in caplog.records:
        assert record.levelname not in ("CRITICAL", "ERROR")


def test_exec_command_cont_output_w_error(caplog):
    template_helper_fn()
    command = ["ls"]
    params = {"ldfs": True, "a": True, "h": True}
    command1 = build_command_list(command, params)
    with pytest.raises(RuntimeError) as err:
        assert exec_command(command1, cont_output=True)

        assert caplog.records[3].levelname == "ERROR"

        assert "The following command has failed: " in str(err.value)


def test_exec_command_w_env_variable(caplog):
    template_helper_fn()
    command = ["echo", "$ENV_VAR"]
    environ = {"ENV_VAR": "Environment Variable Test"}
    exec_command(command, shell=True, environ=environ)

    exp_messages = [
        "Executing command: \n echo $ENV_VAR \n\n",
        "Environment Variable Test\n",
        "Command return code: 0",
    ]
    exp_levelnames = ["INFO", "INFO", "INFO"]
    for i in range(len(caplog.records)):
        assert caplog.records[i].message == exp_messages[i]
        assert caplog.records[i].levelname == exp_levelnames[i]


def test_cont_output_with_linefeeds(capsys):
    """Check that extra line feeds are not added when code prints to stdout"""
    command = ["ls"]
    exec_command(command, shell=True, cont_output=True)
    captured = capsys.readouterr()
    # check that no newline was added
    assert captured.out.split("\n")[1] != ""
