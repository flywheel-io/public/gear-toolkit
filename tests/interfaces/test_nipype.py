import os
import pickle
from unittest import mock

import nipype.pipeline.engine as pe
import pytest
from nipype import config, logging
from nipype.interfaces.base import (
    BaseInterfaceInputSpec,
    File,
    SimpleInterface,
    TraitedSpec,
    traits,
)

from flywheel_gear_toolkit.interfaces.nipype import (
    GearContextInterfaceBase,
    GearContextInterfaceInputSpec,
    GearContextInterfaceOutputSpecBase,
    get_traits_object,
)


@pytest.mark.parametrize(
    "datatype,expected_trait",
    [
        ("boolean", traits.Bool),
        ("string", traits.Str),
        ("context", traits.Str),
        ("integer", traits.Int),
        ("number", traits.Float),
        ("array", traits.List),
        ("file", File),
    ],
)
def test_get_traits_object(datatype, expected_trait):
    obj = get_traits_object(datatype, description="")
    assert isinstance(obj, expected_trait)


def test_get_traits_raised_if_type_matching():
    with pytest.raises(TypeError):
        get_traits_object("type-not-defined")


def test_GearContextInterfaceInputSpec_instantiation():
    input_spec = GearContextInterfaceInputSpec()
    assert hasattr(input_spec, "config_dict")


def test_GearContextInterfaceOutputSpecBase_factory(json_dict):
    with mock.patch.dict("flywheel_gear_toolkit.interfaces.nipype.GLOBALS"):
        manifest = json_dict("manifest")
        output_spec = GearContextInterfaceOutputSpecBase.factory(manifest)()
        assert hasattr(output_spec, "config_config1")
        assert hasattr(output_spec, "config_config2")
        assert hasattr(output_spec, "inputs_dicom")
        assert output_spec.__class__.__name__ == "SampleContextInterfaceOutputSpec"


@pytest.mark.parametrize(
    "name,expected",
    [
        ("my-cool-gear", "MyCoolGearContextInterfaceOutputSpec"),
        (
            "my-!weird-named?-Cool_Gear-2",
            "MyWeirdNamedCoolGear2ContextInterfaceOutputSpec",
        ),
    ],
)
def test_GearContextInterfaceOutputSpecBase(name, expected):
    with mock.patch.dict("flywheel_gear_toolkit.interfaces.nipype.GLOBALS"):
        res = GearContextInterfaceOutputSpecBase.get_class_name({"name": name})
        assert res == expected


def test_nipype_help_command(json_dict):
    with mock.patch.dict("flywheel_gear_toolkit.interfaces.nipype.GLOBALS"):
        manifest = json_dict("manifest")
        fw_gear_context_interface = GearContextInterfaceBase.factory(manifest)
        actual_help_command = fw_gear_context_interface.help(returnhelp=True)

        assert "config_dict" in actual_help_command
        assert "config1" in actual_help_command
        assert "config2" in actual_help_command


def test_nipype_run(json_dict):
    with mock.patch.dict("flywheel_gear_toolkit.interfaces.nipype.GLOBALS"):
        manifest = json_dict("manifest")
        config_dict = json_dict("config")
        fw_gear_context_interface = GearContextInterfaceBase.factory(manifest)()
        fw_gear_context_interface.inputs.config_dict = config_dict
        res = fw_gear_context_interface.run()
        assert "choice1" == res.outputs.config_config1
        assert True is res.outputs.config_config2
        assert "/flywheel/v0/input/dicom/T1w_MPR.dcm.zip" == res.outputs.inputs_dicom


def test_pickling_output_spec(json_dict):
    with mock.patch.dict("flywheel_gear_toolkit.interfaces.nipype.GLOBALS"):
        manifest = json_dict("manifest")
        output_spec = GearContextInterfaceOutputSpecBase.factory(manifest)()
        data = pickle.dumps(output_spec)
        del output_spec
        output_spec = pickle.loads(data)
        assert output_spec.__class__.__name__ == "SampleContextInterfaceOutputSpec"
        assert output_spec._manifest["name"] == "sample"
        assert hasattr(output_spec, "config_config1")
        assert hasattr(output_spec, "config_config2")
        assert hasattr(output_spec, "inputs_dicom")


def test_pickling_interface(json_dict):
    with mock.patch.dict("flywheel_gear_toolkit.interfaces.nipype.GLOBALS"):
        manifest = json_dict("manifest")
        obj = GearContextInterfaceBase.factory(manifest)()
        data = pickle.dumps(obj)
        del obj
        obj = pickle.loads(data)
        assert obj._manifest["name"] == "sample"


# A few classes for the next test. Cannot be defined inside the function scope because
# pickle cannot pickle them if we do so.
class AGearContextInterface(GearContextInterfaceBase):
    def _run_interface(self, runtime):
        self._results["inputs_dicom"] = "A"

        return runtime


class InputSpecB(BaseInterfaceInputSpec):
    x = traits.Str(mandatory=True)


class OutputSpecB(TraitedSpec):
    y = traits.Str()


class ASimpleInterface(SimpleInterface):
    input_spec = InputSpecB
    output_spec = OutputSpecB

    def _run_interface(self, runtime):
        self._results["y"] = "a"

        return runtime


def test_workflow(json_dict, tmpdir):
    cfg = dict(
        execution={
            "stop_on_first_crash": True,
            "hash_method": "content",
            "remove_unnecessary_outputs": False,
            "crashfile_format": "txt",
            "crashdump_dir": os.path.abspath(tmpdir),
        },
    )

    config.update_config(cfg)
    logging.update_logging(config)
    workflowA = pe.Workflow(name="test_workflow", base_dir=tmpdir)
    manifest = json_dict("manifest")
    with mock.patch.dict("flywheel_gear_toolkit.interfaces.nipype.GLOBALS"):
        a = pe.Node(AGearContextInterface.factory(manifest)(), name="interfaceA")
        b = pe.Node(ASimpleInterface(), name="interfaceB")
        a.inputs.config_dict = json_dict("config")
        workflowA.connect([(a, b, [("inputs_dicom", "x")])])
        workflowA.run()
