import logging
from pathlib import Path

import pytest

from flywheel_gear_toolkit.logging import configure_logging

log = logging.getLogger(__name__)


@pytest.fixture(params=["debug", "info", "warning", "error", "critical"])
def get_log_message(request):
    return f'log.{request.param}("Test a {request.param} entry")'


def template_helper_fn(default_config_name="info", update_config=None):
    """
    template_helper_fn takes the same parameters as the configure_logging() function.
    It provides the LogCaptureHandler to the log that was instantiated from the template.

    This assists in running the caplog fixture. In normal operation, this will
    not be required.

    Args:
        default_config_name (str, optional):  Defaults to logging.INFO.
        update_config (str, optional): [description]. Defaults to None.
    """
    if len(logging.root.handlers) == 1:
        root_handler = logging.root.handlers[0]
    else:
        root_handler = logging.root.handlers[1]
        logging.root.removeHandler(root_handler)

    configure_logging(
        default_config_name=default_config_name, update_config=update_config
    )
    root_handler.setLevel(logging.root.level)
    root_handler.setFormatter(logging.root.handlers[0].formatter)
    logging.root.addHandler(root_handler)


def test_templated_info_multiple(get_log_message, caplog):
    with caplog.at_level(logging.INFO):
        template_helper_fn()
        eval(get_log_message)
        for record in caplog.records:
            assert record.levelno >= logging.root.level

        if "debug" in get_log_message:
            assert len(caplog.records) == 0


def test_templated_wrong_level(get_log_message, caplog):
    with caplog.at_level(logging.DEBUG):
        template_helper_fn(default_config_name="debugs")
        eval(get_log_message)
        for record in caplog.records:
            assert logging.root.level == 20
            assert record.levelno >= logging.root.level


def test_templated_debug_multiple(get_log_message, caplog):
    debug_file = Path("job.log")
    with caplog.at_level(logging.DEBUG):
        template_helper_fn(default_config_name="debug")
        eval(get_log_message)
        for record in caplog.records:
            assert record.levelno >= logging.root.level
        # Assert that a debug log file was created
        assert debug_file.exists()
        debug_file.unlink()


def test_templated_set_config():
    config = {"formatters": {"gear": {"format": "[TEST] {levelname:4s} {message}"}}}

    template_helper_fn(update_config=config)

    assert (
        logging.root.handlers[0].formatter._fmt
        == config["formatters"]["gear"]["format"]
    )


def test_info_to_stdout_stderr(capsys):
    configure_logging(default_config_name="info", update_config=None)
    log.debug("Debug.")
    log.info("Info.")
    log.warning("Warning.")
    log.error("Error.")
    log.critical("Critical.")

    captured = capsys.readouterr()

    sysout_tst = ["Log level is INFO", "Info.", "Warning."]
    cap_sysout = captured.out.split("\n")
    for i in range(3):
        assert sysout_tst[i] in cap_sysout[i]

    syserr_tst = ["Error.", "Critical."]
    cap_syserr = captured.err.split("\n")
    for i in range(2):
        assert syserr_tst[i] in cap_syserr[i]


def test_debug_to_stdout_stderr(capsys):
    configure_logging(default_config_name="debug", update_config=None)
    log.debug("Debug.")
    log.info("Info.")
    log.warning("Warning.")
    log.error("Error.")
    log.critical("Critical.")

    captured = capsys.readouterr()

    sysout_tst = ["Log level is DEBUG", "Info.", "Warning."]
    cap_sysout = captured.out.split("\n")
    for i in range(3):
        assert sysout_tst[i] in cap_sysout[i]

    syserr_tst = ["Debug.", "Error.", "Critical."]
    cap_syserr = captured.err.split("\n")
    for i in range(2):
        assert syserr_tst[i] in cap_syserr[i]
