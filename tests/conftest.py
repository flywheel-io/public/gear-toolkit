import functools
import json
import os
import shutil
import tempfile
import zipfile
from pathlib import Path
from unittest.mock import MagicMock

import pytest

TEST_ROOT = Path(__file__).parent
DATA_ROOT = TEST_ROOT / "data"
DICOM_ROOT = DATA_ROOT / "dicom"

pytest_plugins = ("flywheel_gear_toolkit.testing",)


def get_asset_file(root_dir, filename):
    """Returns path to file in assets."""
    if not isinstance(filename, Path):
        filename = Path(filename)
    path = Path(tempfile.mkdtemp())
    shutil.copy(root_dir / filename, path / filename)
    return str(path / filename)


@pytest.fixture
def asset_file():
    get_asset = functools.partial(get_asset_file, TEST_ROOT)
    return get_asset


@pytest.fixture
def dicom_file(asset_file):
    get_asset = functools.partial(get_asset_file, DICOM_ROOT)
    return get_asset


@pytest.fixture(scope="function")
def args_file():
    def get_args_file(name):
        return os.path.join(DATA_ROOT, name + ".txt")

    return get_args_file


@pytest.fixture(scope="function")
def json_file():
    def get_manifest_file(name):
        return str(DATA_ROOT / f"{name}.json")

    return get_manifest_file


@pytest.fixture(scope="function")
def json_dict():
    def get_manifest_dict(name):
        with open(str(DATA_ROOT / f"{name}.json"), "r") as f:
            json_dict = json.load(f)
        return json_dict

    return get_manifest_dict


@pytest.fixture
def zips(tmp_path):
    def inner(file_desc):
        for name, num in file_desc:
            with zipfile.ZipFile(tmp_path / name, "w") as zipf:
                for i in range(num):
                    zipf.writestr(f"test-{i}", b"test")
        return tmp_path

    return inner


@pytest.fixture(autouse=True)
def env_setup(monkeypatch, tmp_path):
    monkeypatch.setenv(
        "FREESURFER_HOME", f"{Path(__file__).parent}/data/opt/freesurfer"
    )


@pytest.fixture
def mock_context(mocker):
    mocker.patch("flywheel_gear_toolkit.context.context.GearToolkitContext")
    gtk_context = MagicMock(autospec=True)
    return gtk_context
