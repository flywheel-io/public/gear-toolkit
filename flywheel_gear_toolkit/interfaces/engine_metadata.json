{
  "$defs": {
    "AcquisitionMetaInput": {
      "properties": {
        "info": {
          "anyOf": [
            {
              "type": "object"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Info"
        },
        "metadata": {
          "anyOf": [
            {
              "type": "object"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Metadata"
        },
        "measurement": {
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Measurement"
        },
        "timestamp": {
          "anyOf": [
            {
              "format": "date-time",
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Timestamp"
        },
        "timezone": {
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Timezone"
        },
        "tags": {
          "default": [],
          "items": {
            "type": "string"
          },
          "title": "Tags",
          "type": "array"
        },
        "files": {
          "default": [],
          "items": {
            "$ref": "#/$defs/FileMetaInput"
          },
          "title": "Files",
          "type": "array"
        },
        "uid": {
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Uid"
        },
        "label": {
          "anyOf": [
            {
              "minLength": 1,
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Label"
        }
      },
      "title": "AcquisitionMetaInput",
      "type": "object"
    },
    "AnalysisMetaInput": {
      "properties": {
        "label": {
          "anyOf": [
            {
              "minLength": 1,
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Label"
        },
        "files": {
          "anyOf": [
            {
              "items": {
                "$ref": "#/$defs/FileMetaInput"
              },
              "type": "array"
            },
            {
              "type": "null"
            }
          ],
          "default": [],
          "title": "Files"
        },
        "timestamp": {
          "anyOf": [
            {
              "format": "date-time",
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Timestamp"
        },
        "tags": {
          "default": [],
          "items": {
            "type": "string"
          },
          "title": "Tags",
          "type": "array"
        },
        "info": {
          "anyOf": [
            {
              "type": "object"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Info"
        }
      },
      "title": "AnalysisMetaInput",
      "type": "object"
    },
    "DeidLogSkipReason": {
      "enum": [
        "unsupported_type",
        "manual_override"
      ],
      "title": "DeidLogSkipReason",
      "type": "string"
    },
    "FileMetaInput": {
      "additionalProperties": false,
      "properties": {
        "name": {
          "title": "Name",
          "type": "string"
        },
        "size": {
          "anyOf": [
            {
              "type": "integer"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Size"
        },
        "type": {
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Type"
        },
        "mimetype": {
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Mimetype"
        },
        "modality": {
          "anyOf": [
            {
              "pattern": "^[ 0-9a-zA-Z_-]{2,64}$",
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Modality"
        },
        "classification": {
          "anyOf": [
            {
              "additionalProperties": {
                "items": {
                  "type": "string"
                },
                "type": "array"
              },
              "type": "object"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Classification"
        },
        "tags": {
          "anyOf": [
            {
              "items": {
                "type": "string"
              },
              "type": "array"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Tags"
        },
        "info": {
          "anyOf": [
            {
              "type": "object"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Info"
        },
        "zip_member_count": {
          "anyOf": [
            {
              "type": "integer"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Zip Member Count"
        },
        "deid_log_id": {
          "anyOf": [
            {
              "pattern": "[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}",
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Deid Log Id"
        },
        "deid_log_skip_reason": {
          "anyOf": [
            {
              "$ref": "#/$defs/DeidLogSkipReason"
            },
            {
              "type": "null"
            }
          ],
          "default": null
        },
        "upload_metadata_type": {
          "const": "file_create_upload_metadata",
          "default": "file_create_upload_metadata",
          "enum": [
            "file_create_upload_metadata"
          ],
          "title": "Upload Metadata Type",
          "type": "string"
        }
      },
      "required": [
        "name"
      ],
      "title": "FileMetaInput",
      "type": "object"
    },
    "ProjectMetaInput": {
      "properties": {
        "files": {
          "anyOf": [
            {
              "items": {
                "$ref": "#/$defs/FileMetaInput"
              },
              "type": "array"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Files"
        },
        "info": {
          "anyOf": [
            {
              "type": "object"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Info"
        },
        "tags": {
          "anyOf": [
            {
              "items": {
                "type": "string"
              },
              "type": "array"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Tags"
        },
        "label": {
          "anyOf": [
            {
              "minLength": 1,
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Label"
        }
      },
      "title": "ProjectMetaInput",
      "type": "object"
    },
    "SessionMetaInput": {
      "properties": {
        "operator": {
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Operator"
        },
        "timestamp": {
          "anyOf": [
            {
              "format": "date-time",
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Timestamp"
        },
        "timezone": {
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Timezone"
        },
        "info": {
          "anyOf": [
            {
              "type": "object"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Info"
        },
        "subject": {
          "anyOf": [
            {
              "$ref": "#/$defs/SubjectMetaInput"
            },
            {
              "type": "null"
            }
          ],
          "default": null
        },
        "age": {
          "anyOf": [
            {
              "type": "integer"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Age"
        },
        "weight": {
          "anyOf": [
            {
              "type": "number"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Weight"
        },
        "tags": {
          "default": [],
          "items": {
            "type": "string"
          },
          "title": "Tags",
          "type": "array"
        },
        "files": {
          "default": [],
          "items": {
            "$ref": "#/$defs/FileMetaInput"
          },
          "title": "Files",
          "type": "array"
        },
        "label": {
          "anyOf": [
            {
              "minLength": 1,
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Label"
        },
        "uid": {
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Uid"
        }
      },
      "title": "SessionMetaInput",
      "type": "object"
    },
    "SubjectMetaInput": {
      "properties": {
        "label": {
          "anyOf": [
            {
              "maxLength": 64,
              "minLength": 1,
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Label"
        },
        "code": {
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Code"
        },
        "project": {
          "anyOf": [
            {
              "pattern": "^[0-9a-fA-F]{24}$",
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Project"
        },
        "master_code": {
          "anyOf": [
            {
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Master Code"
        },
        "info": {
          "anyOf": [
            {
              "type": "object"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Info"
        },
        "age": {
          "anyOf": [
            {
              "type": "integer"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Age"
        },
        "firstname": {
          "anyOf": [
            {
              "pattern": "[^/]{0,64}",
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Firstname"
        },
        "lastname": {
          "anyOf": [
            {
              "pattern": "[^/]{0,64}",
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Lastname"
        },
        "sex": {
          "anyOf": [
            {
              "pattern": "male|female|other|unknown",
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Sex"
        },
        "race": {
          "anyOf": [
            {
              "pattern": "American Indian or Alaska Native|Asian|Native Hawaiian or Other Pacific Islander|Black or African American|White|More Than One Race|Unknown or Not Reported",
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Race"
        },
        "ethnicity": {
          "anyOf": [
            {
              "pattern": "Not Hispanic or Latino|Hispanic or Latino|Unknown or Not Reported",
              "type": "string"
            },
            {
              "type": "null"
            }
          ],
          "default": null,
          "title": "Ethnicity"
        },
        "files": {
          "default": [],
          "items": {
            "$ref": "#/$defs/FileMetaInput"
          },
          "title": "Files",
          "type": "array"
        }
      },
      "title": "SubjectMetaInput",
      "type": "object"
    }
  },
  "additionalProperties": false,
  "properties": {
    "project": {
      "anyOf": [
        {
          "$ref": "#/$defs/ProjectMetaInput"
        },
        {
          "type": "null"
        }
      ],
      "default": null
    },
    "subject": {
      "anyOf": [
        {
          "$ref": "#/$defs/SubjectMetaInput"
        },
        {
          "type": "null"
        }
      ],
      "default": null
    },
    "session": {
      "anyOf": [
        {
          "$ref": "#/$defs/SessionMetaInput"
        },
        {
          "type": "null"
        }
      ],
      "default": null
    },
    "acquisition": {
      "anyOf": [
        {
          "$ref": "#/$defs/AcquisitionMetaInput"
        },
        {
          "type": "null"
        }
      ],
      "default": null
    },
    "analysis": {
      "anyOf": [
        {
          "$ref": "#/$defs/AnalysisMetaInput"
        },
        {
          "type": "null"
        }
      ],
      "default": null
    },
    "type": {
      "anyOf": [
        {
          "type": "string"
        },
        {
          "type": "null"
        }
      ],
      "default": null,
      "title": "Type"
    },
    "upload_metadata_type": {
      "const": "engine_upload_metadata",
      "default": "engine_upload_metadata",
      "enum": [
        "engine_upload_metadata"
      ],
      "title": "Upload Metadata Type",
      "type": "string"
    }
  },
  "title": "EngineUploadMetadata",
  "type": "object"
}