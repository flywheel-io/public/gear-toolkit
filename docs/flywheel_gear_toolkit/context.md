# Gear Toolkit Context

The `GearToolkitContext` is an iteration on the Flywheel SDK's `GearContext`
(`flywheel.GearContext`).

The `flywheel_gear_toolkit.context.GearToolkitContext` class provides an
interface for performing common tasks in the lifecycle of a gear, such as:

* Accessing input files, configuration values, and manifest properties and anSDK client
* Configuring logging
* Writing to output files and output metadata (.metadata.json)

## Basic Usage

`GearToolkitContext` is intended to be used as a context manager to be passed around.
For example, your python script entrypoint (e.g. `run.py`) could look something like:

```python
from flywheel_gear_toolkit import GearToolkitContext
from my_awesome_module import do_something

def main(context):
    # Do something with your input dicom file
    do_something(
        dicom=context.get_input_path('dicom'),
        output_dir=context.output_dir
    )

if __name__ == "__main__":
    with GearToolkitContext() as context:
        # Setup basic logging
        context.init_logging()
        # Call my main function
        main(context)
```

## Accessing gear runtime configuration (config.json)

### Accessing config options

You can the get the job configuration options from the `context.config` dictionary.

```python
# Get the speed option value
my_speed = context.config.get("speed")
```

### Accessing Inputs

You can get the full path to a named input file, or open the file directly:

```python
# Get the path to the input file named 'dicom'
dicom_path = context.get_input_path("dicom")

# Get the input filename
dicom_filename = context.get_input_filename("dicom")

# Open the dicom file for reading
with context.open_input("dicom", "rb") as dicom_file:
  dicom_data = dicom_file.read()

# Get the input file object or value
file_obj = context.get_input_file_object("dicom")
file_info = context.get_input_file_object_value("dicom", "info")
```

### Accessing Destination Container

You can get the destination container and the parent of the destination container
(e.g. to check at which level your analysis gear has been launched) using
`get_destination` and `get_destination_parent` methods. Example:

```python
destination = context.get_destination()
destination_parent = context.get_destination_parent()
```

### Accessing the SDK Client

If your gear is an SDK gear (e.g. has an api-key input), you can easily access
an instance of the Flywheel SDK Client. Example:

```python
# Lookup a project using the client
project = context.client.lookup("my_group/Project 1")
```

## Parsing gear information (manifest.json)

When the `manifest.json` is available in the gear at `/flywheel/v0`, the
`GearToolkitContext` provides an easy access to it. Example:

```python
gear_name = context.manifest.get("name")
gear_version = context.manifest.get("version")
```

## Writing Outputs

### Writing output file

The path to the output directory is available as an attribute on the `context`, and
helper methods exist for opening an output file for writing. By default, this output
directory will point to `/flywheel/v0/output`. Example:

```python
print('Output path: {}'.format(context.output_dir))  # prints "Output path: /flywheel/v0/output"

# Open an output file for writing
with context.open_output('out-file.dcm', 'wb') as f:
  f.write(dicom_data)
```

Notes:

* The path `context.output_dir` is cleaned when the context manager exits with an
exception. To disable this and retain files for inspection on error, enable
debug mode using the [logger](#logging).

### Writing metadata (.metadata.json)

Gears support writing metadata upon job completion back to Flywheel containers _without_
using the SDK.  If a special file `.metadata.json` exists in the output container, the
json object inside will be uploaded as metadata.  Using this you can edit the
destination container and all parent containers of the destination container _without_
needing the SDK details
[.metadata.json spec](https://gitlab.com/flywheel-io/public/gears/-/tree/master/spec#output-metadata)

The `GearToolkitContext` provides a set of methods to help you write the
`.metadata.json`.  Example:

```python
with flywheel_gear_toolkit.GearToolkitContext() as context:
    # Update the destination customer information
    info = {"my-metric": "my-value"}
    context.metadata.update_container(context.destination.type, info=info)

    # Set the modality and classification of an output file
    context.metadata.update_file_metadata(
        "out-file.dcm",  # NOTE: File must exist in output directory fo this to work
        modality="MR",
        classification={"Measurement": ["T1"]}
    )

    # Update the any parent container. Example:
    info = {"my-metric": "my-other-value"}
    context.metadata.update_container("session", label="Session 1", info=info)
```

will generate the following `.metadata.json` file, for an **analysis** gear:

```json

{
    'analysis': {
        'info': {'my-metric': 'my-value'},
        'files': [
            'out-file.dcm': {
                'modality': 'MR',
                'classification': {'Measurement': ['T1']}
            }
        ]
    },
    'session': {
        'info': {"my-metric": "my-other-value"},
        'label': 'Session 1'
    }
}
```

#### Adding a qc result or gear info

GearToolkit also provides helpers to add qc results or gear info to input/output files.

Calling either `context.metadata.add_gear_info` or `context.metadata.add_qc_result` will
populate "gear info" on the destination file:

```python
# Add a QC result for out_file.dcm
context.metadata.add_qc_result('out_file.dcm', "my_qc", state="PASS", {'my-result': 'test'})
```

Doing this on a utility gear with an acquistion destination will produce metadata that
looks like the following, with `<val>` replaced with the values from the specific gear

```
{
    "acquisition": {
        "files": [
        {
            "name": "out_file.dcm",
            "info": {
                "qc": {
                    "<gear-name>": {
                        "job_info": {
                            "version": "<gear-version>",
                            "job_id": "62bc8cbcd98b86a919d60ead",
                            "inputs": {<gear-inputs>},
                            "config": {<gear-config>}
                        },
                        "my_qc": {
                            "state": "PASS",
                            "my-result": "test"
                        }
                    }
                }
            }
        ]
    }
}
```

Notes:

* The `.metadata.json` is cleaned (e.g invalid JSON value), validated and written
  when the context manager exits.
* The value written to the log will truncate long lists.
* By default the metadata will be validated against the schema linked above
  and will mark the gear as failed if its invalid.  But you can turn that off
  by passing `fail_on_validation=False` to the `GearToolkitContext` initiator.

## Logging

Calling `context.init_logging()` will configure python logger to log message
at INFO level. If your `manifest.json` defines a boolean `debug` option, then
it `init_logging()` will use this value to set the logging level to DEBUG when
`debug` is `True`.

```python
with GearToolkitContext() as context:
    # Setup basic logging
    context.init_logging()
```

## SDK Profiling

With `flywheel-sdk` >= 16.0.0, the `GearToolkitContext` context manager
will automatically report out API usage of your gear when `debug` config option
is enabled in the `config.json`. Example:

```python
import logging
log = logging.getLogger(__name__)
from flywheel_gear_toolkit import GearToolkitContext

with GearToolkitContext() as context:
  context.init_logging()
  proj = context.client.lookup('<group>/<project.label>')
  log.info(f"Found project: {proj.id}")
```

will log

```
[ 20210701 12:52:06     INFO flywheel_gear_toolkit.logging: 219 - configure_logging()  ] Log level is DEBUG
[ 20210701 12:52:07     INFO __main__: 4 - <module>()  ] Found project: 603413543321ab021ef0a0a7
[ 20210701 12:52:07    DEBUG flywheel_gear_toolkit.context.context: 618 - __exit__()  ] SDK usage:
{'GET': {'https://ga.ce.flywheel.io:443/api/version': 1},
'POST': {'https://ga.ce.flywheel.io:443/api/lookup': 1}}
```

## Downloading BIDS

If your project or session is fully curated for BIDS, you can download all or a portion
of the project or session to a working directory in BIDS layout.

```python
# Download anat and func files from the project in BIDS format
# bids_path will point to the BIDS folder
bids_path = context.download_project_bids(folders=['anat', 'func'])
```
