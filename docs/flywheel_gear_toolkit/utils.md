# Utils

The utils module `flywheel_gear_toolkit.utils` provides various utilities to assist in
gear development:


## General Utilities

### sdk_post_retry_handler

The function `flywheel_gear_toolkit.utils.sdk_post_retry_handler` enables
retries to SDK methods including `POST` requests, when a transient server error
code is received.  The default SDK is only retrying on `GET, PUT, PATCH`, etc. but
not `POST`.  There are some calls in the Flywheel API that are post requests, but are
safe to retry, namely:

* Updating container info
* Updating file classification
* Adding notes
* Adding tags

If you are using these requests and want to have automatic retry on server errors, you can
use the method like such:

```python
from flywheel_gear_toolkit.utils import sdk_post_retry_handler

# Assuming you already have an SDK client named `client`
acquisition = client.get_acquisition(acq_id)
with sdk_post_retry_handler(client):
    acquisition.update_info(**info)
```


### sdk_delete_404_handler

Sometimes there will be a server error on a delete method, and when
this is retried, the next response can be a 404 which will raise an error.

In order to avoid this, there is a utility `flywheel_gear_toolkit.utils.sdk_delete_404_handler`.
You can use this to ignore these subsequent 404 errors:

```python
from flywheel_gear_toolkit.utils import sdk_delete_404_handler

# Assuming you already have an SDK client named `client`
with sdk_delete_404_handler(client):
    client.delete_acquisition(acq_id)
```


## Manifest

The class `flywheel_gear_toolkit.utils.manifest.Manifest` class provides utilities for
parsing and generating a manifest for a gear. Including functionality to:

* Load a manifest from a file
* Check if the manifest is valid
* Create default config schema from manifest
* Access properties for docker image, version, inputs, license, etc.

Ex.:

```python
>>> from flywheel_gear_toolkit.utils.manifest import Manifest
>>> manifest = Manifest(<manifest_file>)
>>> manifest.is_valid()
[]  # No errors
True
```

## Config

The class `flywheel_gear_toolkit.utils.config.Config` class provides utilities for
parsing and generating a manifest for a gear. Including functionality to:

* Generate default config from manifest
* Access config properties
* Utilities for building a `config.json` file.

Ex.:

```python
>>> from flywheel_gear_toolkit.utils import config, manifest
>>> m = manifest.Manifest(<manifest_file>)
>>> c = config.Config.default_config_from_manifest(m)
>>> c.add_input('dicom','~/input_dicom.dcm.zip')
>>> c.to_json('~/gear/config.json')
```


## Walker

The class `flywheel_gear_toolkit.utils.walker.Walker` class provides a mechanism to
'walk' the flywheel hierarchy. The walker class can be instantiated from any container
(Group, Project, Subject, Session, Acquisition, Analysis) and will recursively walk
through all children, grand-children and so on.

The Walker class is primarily meant to be used with the HierarchyCurator class, the
combination of these allow custom actions to be taken on every container of the same
level under a given container.

An example is show below where the Walker is instantiated at the project level and
actions are taken on all subjects and all sessions in that project:

```python

import flywheel
import os
from flywheel_gear_toolkit.utils.walker import Walker

fw = flywheel.Client(os.environ.get('api_key'))
proj = fw.lookup('testgroup/testproj')

walker = Walker(proj)

for container in walker.walk():
    if container.container_type == 'subject':
        # Do something with subjects
        pass
    elif container.container_type == 'sessions':
        # Do something with sessions
        pass
    else:
        continue
```

The walker has a number of configuration options to control whether or not each
container is reloaded, whether the walking is depth-first or breadth-first, and what
level (if any) to stop at.

### Depth first vs. Breadth first:

By default the Walker uses depth-first traversal, but can also be set to breadth-first traversal. 

NOTE: To see how to configure walker within the HierarchyCurator gear, please see the [documentation 
for the gear](https://gitlab.com/flywheel-io/flywheel-apps/hierarchy-curator#hierarchycurator)

For example if you have the following hierarchy structure:

```bash
sub-01
├── ses-screening
│   ├── acq-anat1
│   │   └── T1w.dicom.zip
│   └── acq-func1
│       └── task1.dicom.zip
└── ses-visit1
   ├── acq-anat1
   │   └── T1w.dicom.zip
   └── acq-func1
       └── task1.dicom.zip
```

Depth-first traversal would look like:

```bash

sub-01                       1.
├── ses-screening            2. 
│   ├── acq-anat1            3.
│   │   └── T1w.dicom.zip    4. 
│   └── acq-func1            5.
│       └── task1.dicom.zip  6.
└── ses-visit1               7.
   ├── acq-anat1             8.
   │   └── T1w.dicom.zip     9.
   └── acq-func1             10.
       └── task1.dicom.zip   11.
```

Whereas breadth-first traversal would look like:

```bash

sub-01                       1.
├── ses-screening            2. 
│   ├── acq-anat1            4.
│   │   └── T1w.dicom.zip    8. 
│   └── acq-func1            5.
│       └── task1.dicom.zip  9.
└── ses-visit1               3.
   ├── acq-anat1             6.
   │   └── T1w.dicom.zip     10.
   └── acq-func1             7.
       └── task1.dicom.zip   11.
```

## Requirements

The requirements module provides only the `install_requirements()` method which allows
for programatically installing requirements.

Generally it is bad practice to programatically install requirements, as whatever the
gear needs to run should be included in the docker container (i.e. installed via `pip`
in the Dockerfile), however there are some use cases where it may be unavoidable, and
this module helps provide that functionality.

ex.

```python
from flywheel_gear_toolkit import GearToolkitContext
from flywheel_gear_toolkit.utils.requirements import install_requirements
import logging
from pathlib import Path

log = logging.getLogger(__name__)
with GearToolkitContext() as gc:
    gc.init_logging()
    req_path = Path(gc.get_input_path('requirements'))
    try:
        install_requirements(req_path)
    except SystemExit as e:
        log.error('Could not install requirements')

```

## Curator

The curator module provides two abstract classes for custom curation. These classes
are meant to be used with the walker class.

* `flywheel_gear_toolkit.utils.curator.HierarchyCurator`: Used to curate an
  entire hierarchy:
    * The `HierarchyCurator` class contains abstract methods to be implemented
      for curating all containers. If you do not wish to curate a container, then simply
      implement each `curate_<container>` method with `pass`.
    * Each container in the flywheel hierarchy has a corresponding `validate` and
      `curate` method in :class:`HierarchyCurator`. These methods should determine whether
      or not to curate each container, and how to curate, respectively. See the
      documentation on :class:`flywheel_gear_toolkit.utils.curator.HierarchyCurator` for
      more details.
    * The specific curator can be configured using the `self.config` dataclass which is
      an instance of `flywheel_gear_toolkit.utils.curator.CuratorConfig`.
    * See more information on the [HierarchyCurator gear documentation](https://gitlab.com/flywheel-io/flywheel-apps/hierarchy-curator)
* `flywheel_gear_toolkit.utils.curator.FileCurator`: Used to curate a single file:
    * The `FileCurator` is very similar to `HierarchyCurator` except it
      only contains methods for validating and curating files since it is only meant to be
      used at the file level.


```python
import flywheel
import os
from flywheel_gear_toolkit.utils import walker, curator

def classify_file(file):
    # Update classification to T1 structural if file name starts with struc_
    if file.name.startswith('struc_'):
        file.update_classification({'Intent':['Structural'], 'Measurement':['T1']})

class myCurator(curator.HierarchyCurator):

    def curate_file(self, file_: flywheel.FileEntry):
        classify_file(file_)

fw = flywheel.Client(os.environ.get('api_key'))

proj = fw.lookup('group/project')

my_walker = walker.Walker(proj)

my_curator = myCurator()

for container in my_walker.walk():
    my_curator.curate_container(container)

```


### HierarchyCurator Config:

In the HierarchyCurator gear, the HierachyCurator has a special attribute called `self.config`
which carries an instance of the `flywheel_gear_toolkit.utils.curator.CuratorConfig`.

This class has the following configuration values which are primarily used in the gear, but some
can also be used in standalone curation scripts.

* Multithreading configuration (only used in HierarchyCurator gear).
    * `multi`: A Boolean value that specifies whether to run in multithreading or
      single-threaded mode. (Default True)
    * `workers`: An integer number that specifies number of worker threads to use.
      (Default 1)
* Walking configuration (only used in HierarchyCurator gear).
    * `depth_first`: Boolean value that results in depth-first (True) curation or
      breadth-first (False) curation.  (Default True).
    * `reload`: Boolean value to either perform `<container>.reload()` on each container
      or not.  (Default False). 
    * `stop_level`: Optional string container level to curate, but not queue any of its
      children.  (Default None)
    * `callback`: An optional function which takes in a container and returns a Boolean.
      If the function returns True for a given container, it's children will be queued,
      otherwise, it's children will NOT be queued.
* Reporting configuration (Can be used in standalone scripts.)
    * `report`: Boolean opting in to reporting.  (Default False).
    * `format`: Optional class containing the typed format of log records.  (Default
      :class:`flywheel_gear_toolkit.utils.reporters.LogRecord`)
    * `path`: Optional `pathlib.Path` object of output path.  (Default
      `Path("/flywheel/v0/output/output.csv")`).

## Reporter

### AggregatedReporter

Class for creating an aggregated reporter during curation. The aggregated reporter logs
errors and messages generated in the curator class to an output json or csv file.
The AggregatedReporter can be adjusted to provide various log formats depending on the
fields required.

The AggregatedReporter is particularly useful when one wants to report on many actions
being taken at multiple levels of the Flywheel hierarchy, such as in a curation gear.

#### General Use:

Once the curator is instantiated, it has the method `append_log` which takes in
keyword arguments corresponding to the columns in a CSV or the Keys in a json blob.

The reporter can be used anywhere, but is meant for use within the
[HierarchyCurator gear](https://gitlab.com/flywheel-io/flywheel-apps/hierarchy-curator)
The gear will instantiate a reporter for you if it is specified in the configuration options
as `self.config.report = True`

Example usage in a standalone curation script

```python

import os
import pathlib
import flywheel
from flywheel_gear_toolkit.utils import curator, reporters, walker

class Curator(curator.HierarchyCurator):

    def __init__(self, **kwargs):
        # Opt in to reporting, reporter will be instantiated automatically.
        self.config.report = True
        self.config.path = Path('./example.csv').resolve()
        super().__init__(**kwargs)

    def curate_session(self, session):
        try:
            raise ValueError      # something that may raise
        except Exception as exc:
            self.reporter.append_log(
                container_type='session',
                label=session.label,
                container_id=session.id,
                resolved=False,
                err=str(exc)
            )

fw = flywheel.Client(os.environ.get('api_key'))

proj = fw.lookup('group/project')

my_walker = walker.Walker(proj)

my_curator = Curator()

for container in my_walker.walk():
    my_curator.curate_container(container)

```

#### Using a different custom logging format:

By default the `AggregatedReporter` uses the format from `LogRecord`, but
it is easy to change this default formatting:

You can create a custom class that adds other fields or utilizes it's own format.

Subclass the `flywheel_gear_toolkit.utils.reporters.BaseLogRecord` class with
the decorator for dataclass and add the fields you want with type annotations:

```python

import dataclasses
from flywheel_gear_toolkit.utils import reporters

@dataclasses.dataclass
class LogRecord(reporters.BaseLogRecord):
    c_type: str = ""
    c_id: str = ""
    label: str = ""
    err: str = ""
    msg: str = ""
    resolved: str = "False"
    search_key: str = ""

reporter = reporters.AggregatedReporter(
    output_path="example.json",
    format=LogRecord
)
```

## Dicom utilities

These utilities are accessible via `flywheel_gear_toolkit.utils.dicom` and require the extra
`dicom` to be installed, see [Optional dependencies](../index.md#optional-dependencies)

The main functions defined here are as follows

### `flywheel_gear_toolkit.utils.dicom.get_dicom_header`

This function extracts the principle header from a single dicom,

*Ex.*

```python
from fw_file.dicom import DICOMCollection
from flywheel_gear_toolkit.utils.dicom import get_dicom_header
import json

dcms = DICOMCollection('/path/to/dicom/archive.dicom.zip')
dcm = dcms[0]
header = get_dicom_header(dcm)
print(json.dumps(header, indent=2))


{
  "FileMetaInformationGroupLength": 216,
  "ImplementationClassUID": "1.2.276.0.7230010.3.0.3.6.5",
  "ImplementationVersionName": "OFFIS_DCMTK_365",
  "MediaStorageSOPClassUID": "1.2.840.10008.5.1.4.1.1.4",
  "MediaStorageSOPInstanceUID": "1.3.12.2.1107.5.2.43.166210.2021071623244310604113077",
  "SourceApplicationEntityTitle": "CHN-Prisma",
  "TransferSyntaxUID": "1.2.840.10008.1.2.1",
  "AcquisitionDate": "20210716",
  ...

```

### `flywheel_gear_toolkit.utils.dicom.get_dicom_array_header`

This function extracts a list of relevant header tags that vary across the collection.

The list of tags that are extracted as arrays can be changed with the function
`flywheel_gear_toolkit.utils.dicom.update_array_tag`

*Ex.*

```python

from fw_file.dicom import DICOMCollection
from flywheel_gear_toolkit.utils.dicom import get_dicom_array_header
import json

dcms = DICOMCollection('/path/to/dicom/archive.dicom.zip')
header = get_dicom_array_header(dcms)
print(json.dumps(header, indent=2))

{
  "AcquisitionNumber": [1, 1, ...],
  "AcquisitionTime": ["231803.980000", "231803.980000", ...],
  "ImagePositionPatient": [
    [-97.948496675929, -126.97630332609, 136.8915681839],
    [-94.0301958383, -127.78061506422, 136.8915681839],
    ...
  ],
  "ImageType": [
    ["ORIGINAL", "PRIMARY", "M", "ND", "NORM"],
    ["ORIGINAL", "PRIMARY", "M", "ND", "NORM"],
    ...
  ],
  "EchoTime": [2.24, 2.24, ...],
  "InstanceNumber": [2, 7, ...],
  "SliceLocation": [-70.415788918056, -66.415789064775, ...],
  "ImageOrientationPatient": [
    [0.20107794190671, 0.97957524533778, 0.0, 0.0, 0.0, -1.0],
    [0.20107794190671, 0.97957524533778, 0.0, 0.0, 0.0, -1.0],
    ...
  ]
}
```


### `flywheel_gear_toolkit.utils.dicom.get_file_info_header`

This function extracts a full header, dicom, dicom_array, and csa header if applicable.

*Ex.*

```python

from fw_file.dicom import DICOMCollection
from flywheel_gear_toolkit.utils.dicom import get_file_info_header
import json

dcms = DICOMCollection('/path/to/dicom/archive.dicom.zip')
header = get_file_info_header(dcms)
print(json.dumps(header, indent=2))

{
  "dicom": {...},
  "dicom_array": {...},
  "csa": {...}
}
```
