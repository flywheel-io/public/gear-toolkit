# Testing

The `flywheel_gear_toolkit.testing` module provides both a [pytest
plugin](https://docs.pytest.org/en/latest/plugins.html) as well as utilities for
profiling gear resource usage.

## Usage

Once you've installed the `flywheel_gear_toolkit`, you can include the testing module as a pytest plugin by including the following in your `conftest.py`

```python
    import pytest
    pytest_plugins = ('flywheel_gear_toolkit.testing',)
```


## Testing Modules


### Hierarchy

This module contains various mocks for the flywheel hierarchy.

The main entrypoint in this module is the
`flywheel_gear_toolkit.testing.hierarchy.fw_project` fixture which accepts
arguments for number of subjects (`n_subjects`), sessions (`n_sessions`), acquisitions
(`n_acquisitions`), and files (`n_files`).  This fixture when called will return a
mocked Flywheel hierarchy which can be used in tests.  Additionally, this provides a
fixture and variable to mock `get_<container>()` methods in this fake hierarchy.  For
example:

`test.py`:

```python
import pytest
pytest_plugins = ('flywheel_gear_toolkit.testing',)

def test_fake_hierarchy(fw_project, sdk_mock, containers):
    # Create mocked project
    project = fw_project(label='test')
    # Mock the sdk get_<container> methods
    for cont_type in ['project', 'subject', 'session', 'acquisition']:
        setattr(sdk_mock, f'get_{cont_type}', containers.get_container)
    sub = project.subjects.find_first()
    print('\n')
    print(sub.name)
    # Use get_subject which is now mocked to return the correct subject.
    sub1 = sdk_mock.get_subject(sub.id)
    print(sub.name)
```

```bash
$ poetry run test.py -s
...
collected 1 item

test.py 

d118db893506f21467bc1d22 sub-0    # Subjects are the same!
d118db893506f21467bc1d22 sub-0
.

```

### SDK

This module contains various mocks and fixtures relating to the flywheel SDK.

#### `flywheel_gear_toolkit.testing.sdk.sdk_mock`

This fixture when included in a method mocks the flywheel SDK. It patches the
`flywheel.Client` class and returns a MagicMock containing all the functions in the SDK.
You can set the return value for anything in the SDK through this interface:

```python
def test_get_acquisition(sdk_mock):
    sdk_mock.get_acquisition.return_value = flywheel.Acquisition(
        {'label':'test'}
    )
    main.run()
    sdk_mock.get_acquisition.assert_called_once()
```

#### `flywheel_gear_toolkit.testing.sdk.get_job`

This fixture mocks the result of getting a job.  You can pass in (optional) a job id,
inputs dictionary, config dictionary, and gear_info.

### Files

These utilies require the optional dependency `fw-file`, see 
[Optional deps](../index.md#optional-dependencies`).

#### `flywheel_gear_toolkit.testing.files.create_dcm`

This meth will create an `fw_file.dicom.DICOM` with the specifified parameters.

You can pass in a custom file, preamble, file metadata, and dicom tag values.

By default `create_dcm` will create a dicom with the following dataset:

```python

>>> from fw_gear_testing.files import create_dcm
>>> dcm = create_dcm()
>>> dcm
DICOM(None)
>>> dcm.dataset.raw
Dataset.file_meta -------------------------------
(0002, 0000) File Meta Information Group Length  UL: 146
(0002, 0001) File Meta Information Version       OB: b'\x00\x01'
(0002, 0002) Media Storage SOP Class UID         UI: MR Image Storage
(0002, 0003) Media Storage SOP Instance UID      UI: 1.2.3
(0002, 0010) Transfer Syntax UID                 UI: Implicit VR Little Endian
(0002, 0012) Implementation Class UID            UI: 1.2.826.0.1.3680043.8.498.1
(0002, 0013) Implementation Version Name         SH: 'PYDICOM 2.1.2'
-------------------------------------------------
(0008, 0016) SOP Class UID                       UI: MR Image Storage
(0008, 0018) SOP Instance UID                    UI: 1.2.3
(0010, 0020) Patient ID                          LO: 'test'
(0020, 000d) Study Instance UID                  UI: 1
(0020, 000e) Series Instance UID                 UI: 1.2
```

You can also pass in any dictionary with the format `{<dicom_tag>:<value>}` to have that
tag added to the dataset.

The `create_dcm` function will try to look up the VR for any tag you pass it, however, you
can also explicitely set the VR and value by passing in a dictionary with the format:

```python

{
    <key>: (<VR>, <value>)
}
```

Ex.:

```python

>>> from fw_gear_testing.files import create_dcm
>>> dcm = create_dcm(**{'ImageOrientationPatient':[0,1,2,3,4,5]})
>>> dcm.get('ImageOrientationPatient')
[0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
```

For more details on the DICOM class works, please see 
[fw-file documentation](https://gitlab.com/flywheel-io/tools/lib/fw-file)



## Gear Profiling Utilities

### Decorators

The `decorators` module provides decorators for profiling gear execution.

#### `flywheel_gear_toolkit.testing.decorators.report_open_fds`

This decorator reports on the number of open File Descriptors before and after a meth is
called.  Mainly this should be used when a suspected socket or open files leak is
happening.

```python

from fw_gear_testing.decorators import report_open_fds
import requests

@report_open_fds(True)
def make_heavy_request(num):
    for i in range(1,num):
        requests.get('https://google.com')
make_heavy_request(10)
```

Output:

```python
...
DEBUG:urllib3.connectionpool:Starting new HTTPS connection (1): www.google.com:443
DEBUG:urllib3.connectionpool:https://www.google.com:443 "GET / HTTP/1.1" 200 None
DEBUG:fw_gear_testing.decorators:Number of fds (make_heavy_request) before: 0, after: 0
```
