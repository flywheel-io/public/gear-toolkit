API
***

Below is a description of the flywheel-gear-toolkit API.

context
-------
.. automodule:: flywheel_gear_toolkit.context.context
   :members:
   :undoc-members:
   :show-inheritance:

   .. automethod::__init__

hpc
-------
.. automodule:: flywheel_gear_toolkit.hpc
   :members:
   :undoc-members:
   :show-inheritance:

   .. automethod::__init__

interfaces
------------
.. automodule:: flywheel_gear_toolkit.interfaces
   :members:
   :undoc-members:
   :show-inheritance:

command_line
^^^^^^^^^^^^
.. automodule:: flywheel_gear_toolkit.interfaces.command_line
   :members: run_classification
   :undoc-members:
   :show-inheritance:

nipype
^^^^^^^^^^^^
.. automodule:: flywheel_gear_toolkit.interfaces.nipype
   :members:
   :undoc-members:
   :show-inheritance:

licenses
--------
.. automodule:: flywheel_gear_toolkit.licenses
   :members:
   :undoc-members:
   :show-inheritance:

Freesurfer
^^^^^^^^^^
.. automodule:: flywheel_gear_toolkit.licenses.freesurfer
   :members:
   :undoc-members:
   :show-inheritance:


logging
-------
.. automodule:: flywheel_gear_toolkit.logging
   :members:
   :undoc-members:
   :show-inheritance:


utils
-----

curator
^^^^^^^
.. automodule:: flywheel_gear_toolkit.utils.curator
   :members:
   :undoc-members:
   :show-inheritance:

   .. automethod::__init__

dicom
^^^^^
.. automodule:: flywheel_gear_toolkit.utils.dicom
   :members:
   :undoc-members:
   :show-inheritance:

file
^^^^
.. automodule:: flywheel_gear_toolkit.utils.file
   :members:
   :undoc-members:
   :show-inheritance:

manifest
^^^^^^^^
.. automodule:: flywheel_gear_toolkit.utils.manifest
   :members:
   :undoc-members:
   :show-inheritance:

qc
^^
.. automodule:: flywheel_gear_toolkit.utils.qc
   :members:
   :undoc-members:
   :show-inheritance:

   .. automethod::__init__

reporters
^^^^^^^^^
.. automodule:: flywheel_gear_toolkit.utils.reporters
   :members:
   :undoc-members:
   :show-inheritance:

   .. automethod::__init__


walker
^^^^^^
.. automodule:: flywheel_gear_toolkit.utils.walker
   :members:
   :undoc-members:
   :show-inheritance:

   .. automethod::__init__

zip_tools
^^^^^^^^^
.. automodule:: flywheel_gear_toolkit.utils.zip_tools
   :members:
   :undoc-members:
   :show-inheritance:

decorators
^^^^^^^^^^
.. automodule:: flywheel_gear_toolkit.testing.decorators
   :members:
   :undoc-members:
   :show-inheritance:

testing
-------

files:
^^^^^^
.. automodule:: flywheel_gear_toolkit.testing.files
   :members:
   :undoc-members:
   :show-inheritance:

gears:
^^^^^^
.. automodule:: flywheel_gear_toolkit.testing.gears
   :members:
   :undoc-members:
   :show-inheritance:

hierarchy:
^^^^^^^^^^
.. automodule:: flywheel_gear_toolkit.testing.hierarchy
   :members:
   :undoc-members:
   :show-inheritance:

sdk:
^^^^
.. automodule:: flywheel_gear_toolkit.testing.sdk
   :members:
   :undoc-members:
   :show-inheritance:

utils:
^^^^^^
.. automodule:: flywheel_gear_toolkit.testing.utils
   :members:
   :undoc-members:
   :show-inheritance:
