# Release Notes

## 0.6.18

__Enhancements__

* Add NA as possible state for add_qc_result

__Bug Fix__

* Update file info even if job id not present

## 0.6.13

__Bug Fix__

* Only Sanitize if dict key is type str

## 0.6.9

__Maintenance__

* Make fw-meta depdency more strict

## 0.6.9

__Enhancements__

* Validation of manifests writes JSON keys in same order as read.

__Maintenance__

* Make fw-file depdency more strict
* Update version of bids-client

## 0.6.8

__Enhancements__

* Make clean_output on error more flexible. Now set through instance attribute.

## 0.6.7

__Enhancements__

* Add clean output dir when exit status is non-zero.
* Sanitize periods in dictionary keys before writing metadata.json

__Bug Fix__

* Validate that `"name"` and `"custom.gear-builder.image"` match in manifest.json

## 0.6.6

__Bug Fix__

* Metadata Fix
* Fix Local Freesurfer Test
* Better check for job-id that relies less on SDK

## 0.6.5

__Enhancements__

* Add metadata helper for adding file tags
* Add file object utilities

## 0.6.4

__Bug Fix__

* Fix `install_freesurfer_license` to search env vars correctly.

## 0.6.3

Breaking change

* Change the manifest keys for FreeSurfer license checking.
The project.info key is now "FREESURFER-LICENSE" or "FREESURFER_LICENSE";  
manifest value for inputs is "freesurfer_license_file"; manifest value for configs is "freesurfer_license_key"

__Enhancements__

* Streamline the QC info methods

## 0.6.2

__Enhancements__

* Add sanitize_name_general, which extends sanitize_filename to process strings. Commonly used in BIDS gears
* Add HPC common methods in flywheel_gear_toolkit.hpc  

## 0.6.1

__Bug Fix__

* Reload files as well as containers in curation.

## 0.6.0

__Maintenance__

* Bump flywheel-bids requirement to 1.## 0.4

## 0.5.9

__Enhancements__

* update_add_gear_info() added to qc, can be used to populate 'gears' section of file info.

## 0.5.8

__Maintenance__

* Expand python requirement to >= python3.7

__Bug Fix__

* Fix validation of manifest for required config values with no default

## 0.5.7

__Maintenance__

* Make Curator reload containers by default.

## 0.5.6

__Bug Fix__

* Fix ``flywheel_gear_toolkit.utils.decorators.report_usage_stats`` missing arguments.

## 0.5.5

__Enhancements__

* Update output file format for `flywheel_gear_toolkit.utils.decorators.report_usage_stats` to allow user to specify the desired file format or generate output as `.png` by default.

## 0.5.4

__Maintenance__

* Add version specifier for `backports.zoneinfo` so it doesn't need to be installed
  with py3.9

## 0.5.3

__Bug Fix__

* Fix `pformat` import.

## 0.5.2

__Bug Fix__

* Fix library for usage with python 3.8.
* Fix behavior of `flywheel_gear_toolkit.utils.config.Config.add_input`.
  to add the proper `<input>.location.name` value.

__Enhancements__

* Update the default fields populated in `flywheel_gear_toolkit.utils.config.Config.add_input`.

## 0.5.1

__Enhancements__

* Add `flywheel_gear_toolkit.utils.sdk_post_retry_handler` and
  `flywheel_gear_toolkit.utils.sdk_delete_404_handler`
  to handle retries for POST/PUT/PATCH/DELETE SDK requests.

__Documentation__

* Improved GearToolkitContext documentation.

## 0.5.0

__Maintenance__

* Move `nipype` to an extra dep, see `optional-deps`
* Move to [qa-ci](https://gitlab.com/flywheel-io/tools/etc/qa-ci/-/tree/master>)

## 0.4.1

__Maintenance__

* Move `flywheel_gear_toolkit.testing.decorators` to `flywheel_gear_toolkit.utils.decorators`.
* Allow `flywheel_gear_toolkit.testing` to be imported without extra dependencies needed by
  specific functions, like `docker` and `dotty_dict`.
* Fix `flywheel_gear_toolkit.utils.config` dependency on `flywheel-sdk`.
* Fix `flywheel_gear_toolkit.testing.files` dependency on `pydicom` for typing.
* Fix `DeprecationWarning` in `flywheel_gear_toolkit.context`.
* Update dependencies, notably fw-file to >=1.## 0.0.

__Enhancements__

* Add SDK usage profiling on debug flag, see `sdk-profiling`.
* Update methods for .metadata.json generation `in GearToolkitContext`
  (e.g. `update_file_metadata`) performs update recursively for dict object by default.
* Add timestamp to log DEBUG message
* Add log level to log INFO message
* Add general usage profiling decorator `flywheel_gear_toolkit.utils.decorators.report_usage_stats`
* Add :func:`flywheel_gear_toolkit.utils.retry_transient_on_post` to allow for retries of POST
on transient server errors.

## 0.4.0

__Enhancements__

* Add `get_input_file_object_value` and `get_input_file_object` to `GearToolkitContext`.
* Add dicom utilities with entrypoints `flywheel_gear_toolkit.utils.dicom.get_dicom_header`,
  `flywheel_gear_toolkit.utils.dicom.get_file_info_header`
* Add testing utilities in `flywheel_gear_toolkit.testing`

__Maintenance__

* Remove `flywheel-sdk` as a dependency, add to optional dependencies, see `optional-deps`
* Make `numpy` an optional dependency

## 0.3.4

__Bug Fix__

* AggregatedReporter
    * Don't store a `multiprocessing.Process` object on class -> Not pickleable.

## 0.3.3

__Enhancements__

* Walker, see `walker`:
    * Change queueing message from info to debug
* Curator, see `curator`:
    * Plug in callback ability for hierarchy curator.
    * Add default configuration options and easy way to configure curator.
    * Add multithreading open input function
    * Add deepcopy hook needed for HierarchyCurator
* AggregatedReporter
    * Add multithreading ability.
* Upon GearToolkitContext exit, zip_member_count for files in output directory get.
* Write .metadata.json when context manager is exiting with sys.exit(## 0..
* Add `get_destination` and `get_destination_parent` to `GearToolkitContext`.

__Maintenance__

* Remove double reporting of errors in `Manifest`.
* Remove testing module and plug in fw-gear-testing instead.
* Bump requirement to python 3.8.
* Change logging format from absolute to relative time.
* Add processName to log.
* Add ability to instantiate new client from GearToolkitContext.

## 0.3.2

__Enhancements__

* Add `config` class to provide interface for config.json.
* Added functionality to generate a customized Nipype Interface to get input and config from geartoolkit context.

## 0.3.1

__Maintenance__

* Make `get_curator` compatible with old version of the custom curator.
* Fix curator, reporter documentation.

## 0.3.0

__Enhancements__

* Walker, see `walker`:
    * Add support to stop at a certain depth.
    * Add support to pass in callback to prevent queueing children
* Update `command_line` API default behavior to return `stdout, stderr, and returncode`,
  when executing command.
* Convert NaN and Inf value to None when writing .metadata.json from context.
* Convert NumPy objects (e.g. `np.array`) to native python objects (e.g. `list`) when
  writing .metadata.json from context.

__Maintenance__

* Update API docs to show class parameters.
* Expose reporters and qc modules.

## 0.2.2

__Maintenance__

* Add `bytes` handler for writing metadata
* Keep logging metadata and writing data in sync (both using json.dumps)

## 0.2.1

__Bug Fix__:

* Allow opting out of SystemExit on .metadata.json validation errors.

## 0.2.0

__Enhancements__:

* Added Support for hierarchy curator and utilities:
    * Create container hierarchy walker, see `walker`
    * Add Manifest class, see `manifest`
    * Add Curator class, see `curator`
    * Add Log Reporter, see `reporter`
    * Add utility to install extra pypi package in Curator class, see `install-requirements`
* Added default .manifest.json jsonschema validation.
* Added automatic parsing of `debug` config value.
* Add default logging of produced metadata when `debug` value is set.

__Maintenance__:

* Add pre-commit to repo.
* Update to poetry from requirements.txt
