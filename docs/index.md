# Introduction

Gear toolkit is a toolkit to make developing gears easier.  Gear toolkit requires at
least ``python-3.8``, and can be install via ``pip`` with ``pip install
flywheel-gear-toolkit`` or ``poetry`` with ``poetry add flywheel-gear-toolkit``.  For
more details see the :ref:`installation`.


## Introduction
`flywheel-gear-toolkit` is a python package maintained by
[Flywheel](https://flywheel.io) It provides a set of modules for facilitating Flywheel
gears development.

## License
`flywheel-gear-toolkit` is developed under an MIT-based
[license](https://gitlab.com/flywheel-io/public/gear-toolkit/blob/main/LICENSE)


## Installation
The package can be installed using pip or poetry using python 3.8 or later.

```bash
pip install flywheel-gear-toolkit
# or
poetry add flywheel-gear-toolkit
```


### Optional Dependencies
This repository contains several optional dependencies that give access to more
functionality.

These extras can be installed via pip or poetry extra syntax: `pip install
"flywheel-gear-toolkit[<extra>]"` or `poetry add flywheel-gear-toolkit -E <extra>`

* `all`: Special meta extra that installs all extras.
* `dicom`: Provides the `fw-file` and `nibabel` packages.
* `fw-file`: Provides the `fw-file` package for interacting with various file types and
  their metadata.
  [fw-file](https://gitlab.com/flywheel-io/tools/lib/fw-file/-/blob/master/README.md)
* `nipype`: Provides the `nipype` and `nibabel` packages
* `numpy`: Provides the `numpy` package. Must be installed for dumping numpy array to
  JSON.
* `sdk`: Provides the `flywheel-sdk` and `flywheel-bids` packages, install this extra if
  you want gear-toolkit to install the SDK and bids utilities, otherwise you can install
  both packages as hard dependencies.  [Flywheel
  SDK](https://flywheel-io.gitlab.io/product/backend/sdk/branches/master/python/index.html)

For development, please refer to the
[README.md](https://gitlab.com/flywheel-io/public/gear-toolkit/blob/master/README.md)


API Key
-------
Some modules in this package rely on the [Flywheel
SDK](https://flywheel-io.gitlab.io/product/backend/sdk/branches/master/python/index.html)
and require a Flywheel API key. You can find and generate your key from the Flywheel
profile page. It will look like this:

![api_key](flywheel_gear_toolkit/images/api-key.png)
