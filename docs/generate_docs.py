"""Mkdocs plugin for FLywheel Gear Toolkit."""
from pathlib import Path

import mkdocs.plugins
import mkdocs.utils

DIR = Path(__file__).parent


class FWPlugin(mkdocs.plugins.BasePlugin):
    """Mkdocs plugin to generate and template CLI docs."""

    # pylint: disable=unused-argument
    def on_serve(self, server, config, builder):  # pylint: disable=no-self-use
        """Watch CLI source dir to enable livereload on code changes."""
        # TODO can remove this once new mkdocs released where it can be
        # configured from config file
        server.watch(f"{DIR.parent}/flywheel_gear_toolkit", builder)
        return server


def asciinema(path, poster="npt:0:0.1", theme="fw", rows="25", base_url=""):
    """Return ascciinema HTML tag."""
    return (
        f'<asciinema-player src="{base_url}/assets/asciinema/{path}" '
        f'poster="{poster}" rows="{rows}" theme="{theme}" />'
    )
