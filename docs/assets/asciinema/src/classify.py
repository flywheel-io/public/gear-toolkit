"""Classify."""
import json
from pathlib import Path

from fw_classification.adapters.dicom import DicomAdapter

dcm_file = Path("~/Downloads/T1w.dcm.zip").expanduser()

profile_path = Path("~/Documents/MR.yaml").expanduser()
dcm_adapter = DicomAdapter(dcm_file)
out = dcm_adapter.classify(profile_path)
print(json.dumps(out["file"]["classification"], indent=2))
