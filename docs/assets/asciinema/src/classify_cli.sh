#!/usr/bin/env bash

echo -ne "\e[1;32m❯\e[0m "
sleep 1
echo "classify --help"
sleep 0.2

classify --help

sleep 0.5
echo -ne "\e[1;32m❯\e[0m "
sleep 1
echo "classify validate ~/Documents/MR.yaml"
sleep 0.3

classify validate ~/Documents/MR.yaml

sleep 0.5
echo -ne "\e[1;32m❯\e[0m "
sleep 1
echo "classify run ~/Documents/MR.yaml ~/Downloads/T1w.dcm.zip -a dicom -o out.json"
sleep 0.3

classify run ~/Documents/MR.yaml ~/Downloads/T1w.dcm.zip -a dicom -o out.json

sleep 0.5
echo -ne "\e[1;32m❯\e[0m "
sleep 1
echo "cat out.json | jq .file.classification"
sleep 0.3

jq .file.classification < out.json
