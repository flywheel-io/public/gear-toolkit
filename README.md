# Flywheel Gear Toolkit

**flywheel-gear-toolkit** is a library that provides tooling for developing Flywheel gears.

## Installation

```
pip install flywheel-gear-toolkit
```

## Documentation

The documentation for the **flywheel-gear-toolkit** can be found
[here](https://flywheel-io.gitlab.io/public/gear-toolkit/index.html).

## Contributing

### Building

The dependency and virtual environment manager for the gear toolkit is [poetry](https://python-poetry.org/).

```
poetry install
```

or

```
poetry install -E all
```

to install with extras.

### Testing, Linting, Building doc

Linting, Testing and Documentation building are all done using `pre-commit`.

After installing poetry, the `pre-commit` command will be installed.  Make sure pre-commit hooks are installed by running either `poetry run pre-commit install` or from within the poetry shell `pre-commit install`.  After hooks are installed, they will automatically be run on each `git commit`, they can all be skipped by running `git commit --no-verify` or specific hooks can be skipped by setting the enviromental variable, ex. `SKIP=test:pre-commit:pytest git commit`.

Individual hooks can also be run independently.  For example, to build sphinx-doc, you can run `pre-commit run publish:doc:test`, or to run black on all files: `pre-commit run test:flywheel-lint -a`.  For a list of all hooks, view the [pre-commit-config](./.pre-commit-config.yaml).

### Managing dependencies

To add new dependencies to this repo, please use [poetry](https://python-poetry.org/)
and to follow the below steps:

```
# Install my-package:
poetry add my-package
# or install my-package as part of the required packages for development (e.g. pytest):
poetry add my-package --dev
# Sync poetry.lock
poetry lock
```

### Building and releasing

#### Local building

To build the project locally and verify if the build was succesful, you can run

```
poetry build --format wheel
twine check dist/*.whl
```

#### Versioning and project information

The `pyproject.toml` file has replaced the usual `setup.py` in this repository and contains information on contributers, maintainers, project description, project URLs, and project version.  In order to change any information on the project, it must be changed in the `pyproject.toml`, file. Documentation for this file can be found [here](https://python-poetry.org/docs/pyproject/), and information on dependency version specification syntax can be found [here](https://python-poetry.org/docs/dependency-specification/)

#### CI for tagging and releasing

There is CI in place to help with tagging and releasing versions and hotfixes of the flywheel-gear-toolkit.

##### Automatic tagging (Not yet enabled)

When a commit that contains the word 'release' is pushed to master or a branch beginning with 'hotfix-' and there are changes to the pyproject.toml file (such as version), gitlab CI will automatically checkout the fix/release, label the branch with the tag found in the current pyproject.toml version, and push the tags.

##### Automatic publishing

When tags are pushed, either manually or by the previous automatic tagging CI, the publish job wil be triggered which will automatically build the project wheel and push the version to PYPI.
